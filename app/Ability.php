<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ability extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];

    // Relations

    public function roles()
    {
    	return $this->belongsToMany('App\Role', 'role_abilities');
    }
}
