<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppSetting extends Model
{
    protected $primaryKey = "key";
    protected $fillable = ['key', 'value'];
    public $timestamps = false;
    public $incrementing = false;
}
