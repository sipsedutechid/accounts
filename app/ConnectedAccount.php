<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConnectedAccount extends Model
{
	protected $fillable = ['email', 'provider', 'oauth_token'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
