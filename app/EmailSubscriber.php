<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailSubscriber extends Model
{
    protected $dates = ['verified_at'];
}
