<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class AccountController extends Controller
{
    public function getStatus(Request $request)
    {
    	$user = new User;
    	$user->status = false;
    	if (!auth()->check())
    		return $user->toJson();

        $user = auth()->user();

    	$user->status = "authenticated";
    	$user->subscription = $user->currentSubscription();

        // dd($user->subscription);
        
        if ($user->subscription) {
            $userTopics = $user->userTopics()->get();
            $accessibles = [];
            $backnumbers = [];
            foreach ($userTopics as $userTopic) {
                if ($userTopic->is_accessible) array_push($accessibles, ['slug' => $userTopic->topic->slug]);
                elseif ($userTopic->is_backnumber) array_push($backnumbers, ['slug' => $userTopic->topic->slug]);
            }
            $user->topics = ['accessible' => $accessibles, 'backnumber' => $backnumbers];
        }

        if ($request->input('request_url'))
            \App\PageView::create(['user_id' => $user->id, 'url' => $request->input('request_url') ?: '']);
    	return response()->json($user);
    }
    public function getTopics()
    {
        $userTopics = auth()->check() ? auth()->user()->userTopics()->select(['is_accessible', 'is_backnumber', 'topic_id'])->get() : [];
        $profession = auth()->check() ? auth()->user()->profession : false;
        if (count($userTopics) == 0) {
            $topics = [];
            if ($profession) {
                $current_index = \App\AppSetting::find('topic.current_index_' . $profession)->value;
                $subscribable_indices = [];
                for ($i=0; $i < 12; $i++) { 
                    array_push($subscribable_indices, $current_index);
                    $current_index = $current_index + 1;
                }
                $subscribable_topics = \App\Topic::where('category', $profession)
                    ->where('post_status', 1)
                    ->whereIn('release', $subscribable_indices)
                    ->orderBy('release', 'desc')
                    ->get();
            } else {
                $current_index_doctor = 1;//\App\AppSetting::find('topic.current_index_doctor')->value;
                $current_index_dentist = 1;//\App\AppSetting::find('topic.current_index_dentist')->value;
                $subscribable_indices = ['doctor' => [], 'dentist' => []];
                for ($i=0; $i < 12; $i++) { 
                    array_push($subscribable_indices['doctor'], $current_index_doctor);
                    array_push($subscribable_indices['dentist'], $current_index_dentist);
                    $current_index_doctor = $current_index_doctor + 1;
                    $current_index_dentist = $current_index_dentist + 1;
                }
                $subscribable_topics = \App\Topic::orWhere(function ($query) use ($subscribable_indices)
                {
                    $query->where('category', 'doctor');
                    $query->where('post_status', 1);
                    $query->whereIn('release', $subscribable_indices['doctor']);
                })->orWhere(function ($query) use ($subscribable_indices)
                {
                    $query->where('category', 'dentist');
                    $query->where('post_status', 1);
                    $query->whereIn('release', $subscribable_indices['dentist']);
                })->orderBy('release')->get();
            }
            
            return json_encode(['accessible' => $subscribable_topics]);
        }

        $topicsArr = ['accessible' => [], 'backnumbers' => []];
        $topics = [];
        foreach ($userTopics as $ut) {
            array_push($topics, $ut->topic);
        }

        $topics = collect($topics);
        $topics = $topics->sortByDesc('release')->toArray();
        foreach ($topics as $topic) {
            if ($topic['post_status'] == 1) {
                foreach ($userTopics as $ut) {
                    if ($ut->topic_id == $topic['id']) {
                        if ($ut->is_accessible) array_push($topicsArr['accessible'], $topic);
                        elseif ($ut->is_backnumber) array_push($topicsArr['backnumbers'], $topic);
                        break;
                    }
                }

            }
        }
        return json_encode($topicsArr);

    }
}
