<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Newsletter;

class EmailSubscriptionController extends Controller
{
    
    public function postSubscribe(Request $request)
    {
    	$this->validate($request, ['email' => 'required|email']);

    	if (!Newsletter::hasMember($request->email)) {
    		Newsletter::subscribe($request->email);
    		return response()->json(['status' => 'success']);
    	}

    	return response()->json(['status' => 'subscribed']);
    }
}
