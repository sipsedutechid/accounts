<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function getSearch(Request $request)
    {
    	return $request->term ? 
    		User::where('name', 'like', '%' . $request->term . '%')
    			->orWhere('email', 'like', '%' . $request->term . '%')
    			->get()
    			->toJson() :
    		abort(404);
    }

    public function getListByUserGroup(Request $request, $group)
    {
    	$group = \App\UserGroup::where('slug', $group)->with('adminUser')->firstOrFail();
    	$members = $request->term ?
    		$group->members()->where('name', 'like', '%' . $request->term . '%')
    			->where('email', 'like', '%' . $request->term . '%')
                ->get() :
    		$group->members()->get();

        foreach ($members as $member) {
            $member->pivot->created_at_label = date(date('Ymd', strtotime($member->pivot->created_at)) == date('Ymd') ? 'g:i a' : 'j M y', strtotime($member->pivot->created_at));
        }
        $group->members = $members;
        return $group->toJson();
    }

    public function postUserGroupAction(Request $request, $group, $action)
    {
        if (!in_array($action, ['add', 'remove', 'adminize'])) abort(404);
        $this->authorize('general', 'accounts.user_groups.manage');

        $user = User::where('email', $request->input('email'))->firstOrFail();
        $group = \App\UserGroup::where('slug', $group)->firstOrFail();

        switch ($action) {
            case 'add': $group->members()->attach($user); break;
            case 'remove': $group->members()->detach($user); break;
            case 'adminize': 
                $group->admin_id = $user->id;
                $group->save();
        }

        return json_encode(['status' => 'success']);
    }
}
