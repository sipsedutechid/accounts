<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserGroup;

class UserGroupController extends Controller
{
    public function postSave(Request $request)
    {
    	$this->authorize('general', 'accounts.user_groups.create');
    	$this->validate($request, [
    		'name' 				=> 'required|min:3|max:255',
    		'members'			=> 'required',
    		'members.*.email'	=> 'required|email|exists:users,email'
    		]);

    	$group = new UserGroup;
    	$group->name = $request->name;
    	$group->slug = str_slug($group->name) . (UserGroup::where('slug', str_slug($group->name))->count() > 0 ? time() : '');
    	$group->description = $request->description;
    	$group->created_by = auth()->user()->id;

    	$membersArr = [];
    	foreach ($request->members as $member) {
    		$id = User::where('email', $member['email'])->firstOrFail()->id;
    		array_push($membersArr, $id);
    		if (isset($member['isAdmin'])) $group->admin_id = $id;
    	}
    	$group->save();
    	$group->members()->attach($membersArr);

    	$request->session()->flash('msg-success', trans('admin.users.groups.messages.create_success'));
    }
}
