<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Image;
use Mail;

class AccountController extends Controller
{

    public function getConnectedAccounts()
    {
        $accounts = auth()->user()->connectedAccounts;
        return view('account.connected_accounts', [
            'accounts'  => $accounts,
            'tab'       => 'accounts'
            ]);
    }

    public function getBilling()
    {
        return view('account.billing', [
            'details'   => auth()->user()->userDetail ?: new \App\UserDetail,
            'tab'       => 'billing',
            ]);
    }

	public function getDoVerify($token, Request $request)
	{
		$user = \App\User::where('verify_token', $token)
			->where('verify_expired_at', '>', time())
			->first();
		
		// Token not found or expired
		if (!$user) {
			return view('account.verify.fail');
		}

		$user->verify();
		auth()->login($user);
		$request->session()->flash('msg-success', trans('account.alerts.verify_complete'));
		return redirect('/my');
	}

    public function getHome(Request $request)
    {
    	return view('account.home', ['user' => auth()->user()]);
    }

    public function getPassword()
    {
        return view('account.password')->with(['tab' => 'password']);
    }

    public function getSettings()
    {
        return view('account.settings')->with(['tab' => 'general']);
    }

    public function getVerify(Request $request)
    {
    	$user = auth()->user();

    	// Already verified
    	if ($user->verified_at) {
    		$user->clearVerify();
    		$request->session()->flash('msg-warning', trans('account.alerts.already_verified'));
    		return redirect('/my');
    	}

        $verifyToken = $user->initVerify();

    	// Send verification email
    	Mail::queue('emails.verify', ['user' => $user, 'token' => $verifyToken], function ($m) use ($user)
    	{
            $m->from('support@gakken-idn.co.id', 'Gakken Support');
    		$m->to($user->email, $user->name)->subject(trans('account.emails.verify_progress'));
    	});

		$request->session()->flash('msg-warning', trans('account.alerts.verify_progress'));
		return redirect('/my');
    }

    public function postBilling(Request $request)
    {
        $this->validate($request, [
            'address_1'     => 'required|max:255',
            'address_2'     => 'max:255',
            'city'          => 'required|max:255',
            'postal_code'   => 'required|max:6',
            'phone'         => 'required|max:13',
            'country_code'  => 'required|max:3',
            ]);

        $details = auth()->user()->userDetail ?: new \App\UserDetail;
        $details->address_1     = $request->address_1;
        $details->address_2     = $request->address_2 ?: null;
        $details->city          = $request->city;
        $details->postal_code   = $request->postal_code;
        $details->phone         = $request->phone;
        $details->country_code  = $request->country_code;
        $details->user_id       = auth()->user()->id;
        $details->save();

        $request->session()->flash('msg-success', trans('account.alerts.billing_updated'));
        return redirect($request->redirect ?: '/my/settings/billing');
    }

    public function postPassword(Request $request)
    {
        $this->validate($request, [
            'status'    => 'required|in:new,change',
            'old'       => 'required_if:status,change',
            'new'       => 'required|min:8|confirmed'
            ]);

        $user = auth()->user();
        if (!$request->status == 'change' && $user->password != bcrypt($request->old)) {
            return redirect('/my/password')->withErrors([trans('account.alerts.wrong_old_password')]);
        }

        $user->password = bcrypt($request->new);
        $user->is_provider = false;
        $user->save();

        // Send notification email
        Mail::queue('emails.password', ['user' => $user], function ($m) use ($user)
        {
            $m->from('support@gakken-idn.co.id', 'Gakken Support');
            $m->replyTo('support@gakken-idn.co.id', 'Gakken Support');
            $m->to($user->email, $user->name);
            $m->subject(trans('account.emails.password_change'));
        });

        $request->session()->flash('msg-success', trans('account.alerts.password_saved'));
        return redirect('/my');
    }

    public function postSettings(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|max:255',
            'avatar'        => 'image|max:2048',
            'profession'    => 'in:doctor,dentist',
            'id_no'         => 'required_with:profession'
            ]);
        $user = auth()->user();
        $user->name = $request->name;
        if ($request->profession) {
            $user->profession = $request->profession;
            $user->id_no = $request->id_no;
        }
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $avatar = Image::make($file);
            $avatar->fit(200, 200);
            $filename = str_random(32) . '_' . time() . '.' . $file->getClientOriginalExtension();
            $avatar->save(public_path() . '/assets/avatars/' . $filename);
            $user->avatar_url = asset('/assets/avatars/' . $filename);
        }
        $user->save();

        $request->session()->flash('msg-success', trans('account.alerts.settings_updated'));
        return redirect('/my/settings');
    }

}
