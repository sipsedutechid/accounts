<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PaymentConfirmation;
use App\SubscriptionOrder;

class OrderController extends Controller
{

    public function deleteConfirmation(Request $request)
    {
        $this->authorize('general', 'accounts.payment_confirmations.delete');
        $pc = PaymentConfirmation::findOrFail($request->id);
        $pc->delete();

        $request->session()->flash('msg-success', 'Payment confirmation data deleted.');
        return redirect($request->redirect ?: '/orders/confirmations/all');
    }

    public function getConfirmationsList(Request $request)
    {
        $this->authorize('general', 'accounts.payment_confirmations.view');
        $pcs = $request->s ?
            PaymentConfirmation::where('order_no', 'like', '%' . $request->s . '%')->orderBy('created_at', 'desc')->paginate(15) :
            PaymentConfirmation::orderBy('created_at', 'desc')->paginate(15);

        return view('orders.list_confirmations')->with(['pcs' => $pcs]);
    }

    public function getList(Request $request)
    {
    	$this->authorize('general', 'accounts.orders.view');
		$orders = $request->s ?
			SubscriptionOrder::where('order_no', 'like', '%' . $request->s . '%')->orderBy('order_no', 'desc')->paginate(15) :
			SubscriptionOrder::orderBy('order_no', 'desc')->paginate(15);

    	return view('orders.list')->with(['orders' => $orders]);
    }

    public function getSingle($orderNo, Request $request)
    {
    	$this->authorize('general', 'accounts.orders.view');
    	$order = SubscriptionOrder::where('order_no', $orderNo)->firstOrFail();

    	return view('orders.single')->with(['order' => $order]);
    }

    public function postAction($action, Request $request)
    {
        $this->validate($request, [
            'pre_order_no'  => 'required|exists:subscription_orders,order_no',
            'order_no'      => 'required|same:pre_order_no'
            ]);
        $order = SubscriptionOrder::where('order_no', $request->order_no)->firstOrFail();

        if ($action == 'approve') {
            $this->authorize('general', 'accounts.orders.approve');
            $order = $order->approve();
            $request->session()->flash('msg-success', trans('admin.orders.messages.approved'));
            return redirect('/orders/' . $request->order_no . '?ref=' . ($request->ref ?: 'list'));
        }
        if ($action == 'delete') {
            $this->authorize('general', 'accounts.orders.delete');
            if (!in_array($order->status, ['success', 'settled'])) {
                $order->delete();
                $request->session()->flash('msg-success', trans('admin.orders.messages.deleted'));
            } else $request->session()->flash('msg-danger', trans('admin.orders.messages.cannot_delete'));
            return redirect('/orders/all');
        }

    }
}
