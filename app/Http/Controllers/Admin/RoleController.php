<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Ability;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;

class RoleController extends Controller
{
	public function getCreate()
	{
    	$this->authorize('general', 'accounts.roles.create');
		return view('users.roles.form', [
			'abilities'	=> Ability::all(),
			'role' 		=> new Role
			]);
	}

	public function getEdit($roleId)
	{
    	$this->authorize('general', 'accounts.roles.edit');
    	$role = Role::findOrFail($roleId);
		return view('users.roles.form', [
			'abilities'	=> Ability::all(),
			'role' 		=> $role
			]);
	}

    public function getList(Request $request)
    {
    	$this->authorize('general', 'accounts.roles.view');

    	$roles = $request->s ?
            Role::where('name', 'like', '%' . $request->s . '%')
                ->paginate(15):
            Role::paginate(15);
    	return view('users.roles.list')->with(['roles' => $roles]);
    }

    public function postCreate(Request $request)
    {
    	$this->authorize('general', 'accounts.roles.create');
    	$this->validate($request, [
    		'name'			=> 'required|max:255',
    		'abilities.*'	=> 'required|exists:abilities,id'
    		]);

    	$role = new Role;
    	$role->name = $request->name;
    	$role->save();

    	$role->abilities()->sync($request->input('abilities.*'));

    	$request->session()->flash('msg-success', trans('admin.roles.messages.success_new'));
    	return redirect('/roles/all');
    }

    public function postEdit(Request $request)
    {
    	$this->authorize('general', 'accounts.roles.edit');

    	$this->validate($request, [
    		'role'			=> 'required|integer|exists:roles,id',
    		'name'			=> 'required|max:255',
    		'abilities.*'	=> 'required|exists:abilities,id'
    		]);

    	$role = Role::findOrFail($request->role);
    	$role->name = $request->name;
    	$role->save();

    	$role->abilities()->sync($request->input('abilities.*'));

    	$request->session()->flash('msg-success', trans('admin.roles.messages.success_edit'));
    	return redirect('/roles/all');
    }

}
