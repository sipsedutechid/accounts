<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\UserSubscription;
use Mail;

class UserController extends Controller
{
    public function getCreate()
    {
    	$this->authorize('general', 'accounts.users.create');
    	return view('users.form', ['user' => new User]);
    }

    public function getEdit($email)
    {
        $this->authorize('general', 'accounts.users.edit');
        $user = User::where('email', $email)->firstOrFail();
        if ($user->role->is_super)
            return app()->abort(404);
        $user->role = $user->role_id;
        return view('users.form', [
            'user'          => $user,
            'subscription'  => $user->currentSubscription(),
            'orders'        => $user->subscriptionOrders,
            ]);
    }

    public function getList(Request $request)
    {
        $this->authorize('general', 'accounts.users.view');
        $users = $request->s ?
            User::where('name', 'like', '%' . $request->s . '%')
                ->orWhere('email', 'like', '%' . $request->s . '%')
                ->withCount('userGroups')
                ->paginate(15):
            User::withCount('userGroups')->paginate(15);
        return view('users.list')->with(['users' => $users]);
    }

    public function getSubscribe(Request $request, $email)
    {
        $this->authorize('general', 'accounts.users.subscription.subscribe');
        $user = User::where('email', $email)->firstOrFail();
        return view('users.form_subscribe')->with(['user' => $user]);
    }

    public function postCreate(Request $request)
    {
    	$this->authorize('general', 'accounts.users.create');
    	$this->validate($request, [
    		'email'					=> 'required|email|max:255',
            'password'              => 'required|min:8|max:255|confirmed',
            'name'                  => 'required|max:255',
            'role'                  => 'required|integer|exists:roles,id',
            'autoverify'            => 'boolean',
            'send_welcome_email'    => 'boolean',
            'redirect'              => 'in:list,home'
            ]);

        $user = new User;
        $user->email    = $request->email;
        $user->password = bcrypt($request->password);
        $user->name     = $request->name;
        $user->role_id  = $request->role;
        $user->save();
        $user->toMoodleUser();

        if ($request->send_welcome_email) {
            Mail::queue('emails.welcome', ['user' => $user], function ($m) use ($user)
            {
                $m->from('support@gakken-idn.co.id', 'Gakken Support');
                $m->to($user->email, $user->name)->subject(trans('account.emails.welcome'));
            });
        }

        if ($request->autoverify) 
            $user->verify();
        else {
            $verifyToken = $user->initVerify();
            Mail::queue('emails.' . app()->getLocale() . '.account_verify', ['user' => $user, 'token' => $verifyToken], function ($m) use ($user)
            {
                $m->from('support@gakken-idn.co.id', 'Gakken Support');
                $m->to($user->email, $user->name)->subject(trans('account.emails.verify_progress'));
            });
        }

        $request->session()->flash('msg-success', trans('admin.users.register.messages.success_new'));
        switch ($request->redirect) {
            case 'list': $redirect = '/users/all'; break;
            default: $redirect = '/my';
        }
        return redirect($redirect);
    }

    public function postEdit(Request $request)
    {
        $this->authorize('general', 'accounts.users.edit');
        $this->validate($request, [
            'email'         => 'required|exists:users,email',
            'name'          => 'required|max:255',
            'password'      => 'confirmed|min:8',
            'role'          => 'required|integer|exists:roles,id',
            'autoverify'    => 'boolean',

            'profession'    => 'in:doctor,dentist',
            'id_no'         => 'required_with:profession|max:24',
            'address_1'     => 'required_with:address_2|max:255',
            'address_2'     => 'max:255',
            'city'          => 'max:255',
            'postal_code'   => 'max:6',
            'phone'         => 'max:13',
            'country'       => 'max:3',
            ]);

        $user = User::where('email', $request->email)->firstOrFail();
        $user->name     = $request->name;
        $user->role_id  = $request->role;
        if ($request->password) {
            $user->password = bcrypt($request->password);
            $user->is_provider = false;
        }
        $user->profession = $request->profession ?: null;
        $user->id_no = $request->id_no ?: null;
        $user->save();

        $detail = $user->userDetail ?: new \App\UserDetail;
        $detail->user_id = $user->id;
        $detail->address_1 = $request->address_1;
        $detail->address_2 = $request->address_2;
        $detail->city = $request->city;
        $detail->postal_code = $request->postal_code;
        $detail->phone = $request->phone;
        $detail->country_code = $request->country;
        $detail->save();

        if ($request->autoverify)
            $user->verify();

        $request->session()->flash('msg-success', trans('admin.users.register.messages.success_edit'));
        return redirect('users/all');
    }

    public function postSubscribe(Request $request)
    {
        $this->authorize('general', 'accounts.users.subscription.subscribe');
        $this->validate($request, [
            'email'         => 'required|exists:users,email',
            'fixed_dates'   => 'boolean',
            'starts_at'     => 'required_with:fixed_dates|date|after:yesterday',
            'expired_at'    => 'required_with:fixed_dates|date|after:starts_at',
            'duration'      => 'required_without:fixed_dates|integer',
            'period'        => 'required_without:fixed_dates|in:days,weeks,months,years'
            ]);

        $user = User::where('email', $request->email)->firstOrFail();
        $subscription = \App\Subscription::where('is_custom', true)->firstOrFail();
        $currentSubscription = $user->currentSubscription();
        $userSubscription = new UserSubscription;
        $userSubscription->user_id = $user->id;
        $userSubscription->subscription_id = $subscription->id;
        
        if ($request->fixed_dates) {
            $userSubscription->starts_at = strtotime($request->starts_at);
            $userSubscription->expired_at = strtotime($request->expired_at);
        } else {
            $startDate = $currentSubscription ? strtotime($currentSubscription->expired_at) : time();
            $userSubscription->starts_at = $startDate;
            $userSubscription->expired_at = strtotime('+' . $request->duration . ' ' . $request->period, $startDate);
        }
        $userSubscription->save();

        $request->session()->flash('msg-success', trans('admin.users.subscribe.messages.success_new'));
        return redirect('/users/' . urlencode($user->email));
    }
}
