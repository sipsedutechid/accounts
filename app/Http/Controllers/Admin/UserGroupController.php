<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserGroup;

class UserGroupController extends Controller
{
    public function getCreate()
    {
        $this->authorize('general', 'accounts.user_groups.create');
        return view('users.groups.form')->with(['group' => new UserGroup]);
    }

    public function getList(Request $request)
    {
        $this->authorize('general', 'accounts.user_groups.view');
        $groups = $request->s ?
            UserGroup::where('name', 'like', '%' . $request->s . '%')
            	->orderBy('name')
            	->withCount('members')
                ->paginate(15):
            UserGroup::orderBy('name')->withCount('members')->paginate(15);
        return view('users.groups.list')->with(['groups' => $groups]);
    }

    public function getSingle(Request $request, $slug)
    {
        $this->authorize('general', 'accounts.user_groups.view');
        $group = UserGroup::where('slug', $slug)->withCount('members')->firstOrFail();

        return view('users.groups.single')->with([
            'group'     => $group
            ]);
    }

    public function getSubscribe(Request $request, $slug)
    {
        $this->authorize('general', 'accounts.users.subscription.subscribe');
        $group = UserGroup::where('slug', $slug)->firstOrFail();
        return view('users.form_subscribe', ['group' => $group]);
    }

    public function deleteSingle(Request $request, $slug)
    {
        $this->authorize('general', 'accounts.user_groups.delete');
        $group = UserGroup::where('slug', $slug)->firstOrFail();
        $group->slug = $group->slug . '/' . time();
        $group->save();
        $group->delete();

        $request->session()->flash('msg-success', trans('admin.users.groups.messages.delete_success'));
        return redirect('/users/groups/all');
    }

    public function postSubscribe(Request $request)
    {
        $this->authorize('general', 'accounts.users.subscription.subscribe');
        $this->validate($request, [
            'group'         => 'required|exists:user_groups,slug',
            'fixed_dates'   => 'boolean',
            'starts_at'     => 'required_with:fixed_dates|date|after:yesterday',
            'expired_at'    => 'required_with:fixed_dates|date|after:starts_at',
            'duration'      => 'required_without:fixed_dates|integer',
            'period'        => 'required_without:fixed_dates|in:days,weeks,months,years'
            ]);

        $users = UserGroup::where('slug', $request->group)->firstOrFail()->members;
        $subscription = \App\Subscription::where('is_custom', true)->firstOrFail();

        foreach ($users as $user) {
            $currentSubscription = $user->currentSubscription();
            $userSubscription = new \App\UserSubscription;
            $userSubscription->user_id = $user->id;
            $userSubscription->subscription_id = $subscription->id;
            
            if ($request->fixed_dates) {
                $userSubscription->starts_at = strtotime($request->starts_at);
                $userSubscription->expired_at = strtotime($request->expired_at);
            } else {
                $startDate = $currentSubscription ? strtotime($currentSubscription->expired_at) : time();
                $userSubscription->starts_at = $startDate;
                $userSubscription->expired_at = strtotime('+' . $request->duration . ' ' . $request->period, $startDate);
            }
            $userSubscription->save();
        }

        $request->session()->flash('msg-success', trans('admin.users.subscribe.messages.success_new'));
        return redirect('/users/groups/' . urlencode($request->group));
    }

}
