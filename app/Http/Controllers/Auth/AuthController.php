<?php

namespace App\Http\Controllers\Auth;

use App\ConnectedAccount;
use App\User;
use Mail;
use Socialite;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectAfterLogout = "";
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/my';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => ['logout', 'postLogin']]);
        $this->redirectAfterLogout = env('ROOT_APP_URL', '/login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name'                  => 'required|max:255',
            'email'                 => 'required|email|max:255|unique:users',
            'password'              => 'required|min:6|confirmed',
            'agreement'             => 'accepted',
            'g-recaptcha-response'  => 'required'
        ]);
        $validator->after(function ($validator) use ($data)
        {
            $recaptcha = new \ReCaptcha\ReCaptcha(env('GOOGLE_RECAPTCHA_SECRET'));
            $resp = $recaptcha->verify($data['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
            if (!$resp->isSuccess()) {
                $validator->errors()->add('recaptcha', 'Recaptcha Error');
            }
        });

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'password'  => bcrypt($data['password']),
            'role_id'   => \App\Role::where('is_user', true)->firstOrFail()->id
        ]);
        $user->toMoodleUser();
        // Send welcome email
        Mail::queue('emails.welcome', ['user' => $user], function ($m) use ($user)
        {
            $m->from('support@gakken-idn.co.id', 'Gakken Support');
            $m->to($user->email, $user->name)->subject(trans('account.emails.welcome'));
        });

        $verifyToken = $user->initVerify();

        // Send verification email
        Mail::queue('emails.verify', ['user' => $user, 'token' => $verifyToken], function ($m) use ($user)
        {
            $m->from('support@gakken-idn.co.id', 'Gakken Support');
            $m->to($user->email, $user->name)->subject(trans('account.emails.verify_progress'));
        });

        request()->session()->flash('msg-warning', trans('account.alerts.verify_progress'));
        return $user;
    }

    /**
     * Authenticated user handling
     */
    protected function authenticated(\Illuminate\Http\Request $request, $user)
    {
        if ($request->wantsJson()) {
            return response()->json([
                'authenticated' => true,
                'intended'      => $request->redirect ?: \URL::previous() ?: url()
                ]);
        }

        return redirect()->intended($request->redirect ?: $this->redirectPath());
    }

    /**
     * @Override
     * Get the failed login response instance.
     */
    protected function sendFailedLoginResponse(\Illuminate\Http\Request $request)
    {
        if ($request->wantsJson()) {
            return response()->json([
                'authenticated' => false,
                'error'         => $this->getFailedLoginMessage()
                ]);
        }

        return redirect('/login?redirect=' . $request->redirect)
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([$this->getFailedLoginMessage()]);
    }

    public function redirectToProvider($provider, \Illuminate\Http\Request $request)
    {
        if ($request->input('redirect'))
            $request->session()->put('redirect', $request->input('redirect'));
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider, \Illuminate\Http\Request $request)
    {
        if (!in_array($provider, ['google', 'facebook']))
            return app()->abort(404);

        $providerUser = Socialite::driver($provider)->user();
        
        // Check if account has been associated
        $connectedAccount = ConnectedAccount::where('provider', $provider)
            ->where('email', $providerUser->getEmail())
            ->first();
        if ($connectedAccount) {
            auth()->login($connectedAccount->user);
            return redirect()->intended('/my');
        }

        // Check if an account exists with the same email
        $user = User::where('email', $providerUser->getEmail())->first();
        if ($user) {
            $connectedAccount = new ConnectedAccount([
                'email'         => $providerUser->getEmail(),
                'provider'      => $provider,
                'oauth_token'   => $providerUser->token,
                ]);
            $user->connectedAccounts()->save($connectedAccount);
            $user->verify();
            auth()->login($user);
            return redirect()->intended('/my');
        }

        // New user registration
        $user = new User;
        $user->email        = $providerUser->getEmail();
        $user->password     = bcrypt(str_random(128));
        $user->name         = $providerUser->getName();
        $user->avatar_url   = str_replace('sz=50', 'sz=200', $providerUser->getAvatar());
        $user->role_id      = \App\Role::where('is_user', true)->firstOrFail()->id;
        $user->is_provider  = true;
        $user->save();
        $user->verify();
        $user->toMoodleUser();

        // Send welcome email
        Mail::queue('emails.welcome', ['user' => $user], function ($m) use ($user)
        {
            $m->from('support@gakken-idn.co.id', 'Gakken Support');
            $m->to($user->email, $user->name)->subject(trans('account.emails.welcome'));
        });

        $connectedAccount = new ConnectedAccount([
            'email'         => $providerUser->getEmail(),
            'provider'      => $provider,
            'oauth_token'   => $providerUser->token,
            ]);
        $user->connectedAccounts()->save($connectedAccount);
        
        auth()->login($user);

        if ($request->session()->has('redirect')) {
            $redirectUrl = $request->session()->pull('redirect');
            return redirect($redirectUrl);
        };
        return redirect()->intended('/my');
    }

}
