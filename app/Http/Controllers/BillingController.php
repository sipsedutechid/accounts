<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\PaymentConfirmation;
use App\Subscription;
use App\SubscriptionOrder;
use App\Veritrans\Veritrans;
use View;

class BillingController extends Controller
{
    public function __construct()
    {   
        Veritrans::$serverKey = env('VT_SERVER_KEY', '');
        Veritrans::$isProduction = env('VT_IS_PRODUCTION', false);
    }

    public function getInvoice($orderNo, Request $request)
    {
        $order = SubscriptionOrder::where('order_no', $orderNo)->firstOrFail();
        if ($order->user->id != auth()->user()->id)
            $this->authorize('accounts.orders.view_invoice');

        if (!in_array($order->status, ['success', 'settled']))
            return app()->abort(404);

        $invoiceHtml = View::make('printables.invoice')->with(['order' => $order])->render();
        $options = new \Dompdf\Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new \Dompdf\Dompdf($options);
        $pdf->loadHtml($invoiceHtml);
        $pdf->setPaper('A4', 'portrait');
        $pdf->setBasePath(env('APP_URL', 'https://accounts.gakken-idn.id'));
        $pdf->render();
        $pdf->stream('Gakken Invoice - Order No. ' . $order->order_no, ['Attachment' => 0]);
    }

    public function getOrderConfirm()
    {
        return view('orders.confirm');
    }

    public function postOrderConfirm(Request $request)
    {
        $this->validate($request, [
            'order_no'      => 'required|exists:subscription_orders,order_no',
            'bank'          => 'required',
            'account_no'    => 'required',
            'account_name'  => 'required',
            'to_account'    => 'required',
            'transfer_date' => 'required|date',
            'amount'        => 'required|integer'
            ]);

        $pc = new PaymentConfirmation;
        $pc->order_no       = $request->order_no;
        $pc->bank           = $request->bank;
        $pc->account_no     = $request->account_no;
        $pc->account_name   = $request->account_name;
        $pc->to_account     = $request->to_account;
        $pc->ref_no         = $request->ref_no;
        $pc->transfer_date  = $request->transfer_date;
        $pc->amount         = $request->amount;
        $pc->created_by     = auth()->user()->id;
        $pc->save();

        $request->session()->flash('msg-success', 'Payment confirmation submitted. Our staff will confirm your payment within 2x24 hours.');
        return redirect('/my');
    }

    public function vtNotification()
    {
        $vt = new Veritrans;
        $json_result = file_get_contents('php://input');
        $result = json_decode($json_result);

        if ($result) {
            $notif = $vt->status($result->order_id);
        }
        
        $transaction    = $notif->transaction_status;
        $type           = $notif->payment_type;
        $order_id       = $notif->order_id;
        $fraud          = $notif->fraud_status;

        $order = SubscriptionOrder::where('order_no', $order_id)->first();
        if (!$order) return app()->abort(404);

        if ($transaction == 'capture') {
            if ($type == 'credit_card'){
                if ($fraud == 'challenge'){
                    $order->status = "challenge";
			        $order->save();
                } else {
                    $order->status = "success";
			        $order->save();
		            $order->intoSubscription();
                }
            }
        }
        else if ($transaction == 'settlement'){
	        $order->settle();
            $order->intoSubscription();
        } 
        else if($transaction == 'pending'){
	        $order->pend();
        } 
        else if ($transaction == 'deny') {
	        $order->deny();
        }
    }

    public function postSubscribe(Request $request)
    {
    	$this->validate($request, [
            'subscription'          => 'required|exists:subscriptions,name',
            'method'                => 'required|in:vtweb,vtwebinstallment,banktransfer',
            'g-recaptcha-response'  => 'required'
			]);

        $recaptcha = new \ReCaptcha\ReCaptcha(env('GOOGLE_RECAPTCHA_SECRET'));
        $resp = $recaptcha->verify($request->input('g-recaptcha-response'), $_SERVER['REMOTE_ADDR']);
        if (!$resp->isSuccess()) {
            return redirect()->back()->withErrors(['Captcha Error'])->withInput();
        }

		$subscription = Subscription::where('name', $request->subscription)->firstOrFail();
        $quantity = 1;

		$order = new SubscriptionOrder;
		$order->order_no 		= SubscriptionOrder::generateOrderNumber();
		$order->user_id 		= auth()->user()->id;
		$order->subscription_id = $subscription->id;
		$order->quantity 		= $quantity;
		$order->total 			= $subscription->price * $quantity;
        $order->method          = $request->method;
		$order->client_ip		= $request->ip();
        $order->expired_at      = time() + 60 * 60 * 24 * 7; // Order data expires after 7 days
		$order->save();

        if ($request->method == 'banktransfer') {
            $request->session()->flash('msg-success', trans('subscription.self.orders.order_received', ['subscription' => $subscription->label]));
            return redirect('/my');
        }

		$vt = new \App\Veritrans\Veritrans;

        $transaction_details = [
            'order_id'      => $order->order_no,
            'gross_amount'  => $order->total,
        ];

        $userDetail = auth()->user()->userDetail;
        $userNames = explode(" ", auth()->user()->name);
        $userLastName = array_pop($userNames);
        $userFirstName = implode(" ", $userNames);
        $address = [
            'first_name'    => $userFirstName,
            'last_name'     => $userLastName,
            'address'       => $userDetail->address1 . ' ' . $userDetail->address2,
            'city'          => $userDetail->city,
            'postal_code'   => $userDetail->postal_code,
            'phone'         => $userDetail->phone,
            'country_code'  => $userDetail->country_code,
            ];

        $customer_details = [
            'first_name'        => $userFirstName,
            'last_name'         => $userLastName,
            'email'             => auth()->user()->email,
            'phone'             => $userDetail->phone,
            'billing_address' 	=> $address,
            'shipping_address'	=> $address
        ];

        $vtweb = ['credit_card_3d_secure' => true];
        if ($request->method == "vtwebinstallment") {
            $vtweb['enabled_payments'] = ['credit_card'];
            $vtweb['payment_options'] = [
                'installment' => [
                    'required'          => true,
                    'installment_terms' => [
                        'bni'       => [3, 6, 12],
                        'mandiri'   => []
                    ]
                ]
            ];
        }

        $transaction_data = [
            'payment_type'          => 'vtweb', 
            'vtweb'					=> $vtweb,
            'transaction_details'	=> $transaction_details,
            'item_details'          => [[
            	'id'		=> 'SUB-' . strtoupper($subscription->name),
                'price'     => $subscription->price,
                'quantity'  => $quantity,
                'name'      => $subscription->label,
            ]],
            'customer_details'   	=> $customer_details
        ];
    
        try {
            $vtweb_url = $vt->vtweb_charge($transaction_data);
            return redirect($vtweb_url);
        } 
        catch (Exception $e) {   
            return $e->getMessage;
        }
    }

}
