<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Subscription;
use App\SubscriptionOrder;

class SubscriptionController extends Controller
{

    public function getList()
    {
        $current = auth()->user()->currentSubscription();
        $orders = auth()->user()->subscriptionOrders;
        $userSubscriptions = auth()->user()->userSubscriptions;
        return view('account.subscription.list', [
            'current'  			=> $current,
            'orders'        	=> $orders,
            'userSubscriptions'	=> $userSubscriptions
            ]);
    }

	public function getSubscribe(Request $request)
	{
		if (!isset(auth()->user()->profession)) {
			$request->session()->flash('msg-warning', trans('account.alerts.cannot_subscribe'));
			return redirect('/my/settings');
		}
		if ($current = auth()->user()->currentSubscription()) {
			if (time() < strtotime('-3 months', strtotime($current->expired_at))) {
				$request->session()->flash('msg-warning', trans('account.alerts.cannot_subscribe_has_current'));
				return redirect('/my');
			}
		}

		return view('subscribe.checkout')->with([
			'subscriptions' => \App\Subscription::where('name', '<>', 'custom')->get()
			]);
	}
	
}
