<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TestController extends Controller
{
	public function getTest()
	{
		return "Test";
	}

    public function getEmail($view)
    {
    	return view($view, [
    		'user'	=> \App\User::find(1),
            'token' => str_random(32)
    		]);
    }
    public function curl()
    {
        echo \App\User::find(2)->toMoodleUser();
    }
}
