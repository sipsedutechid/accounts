<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Cookie;

class SetAuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (Auth::check())
            $response->cookie('gkauth_token', str_random(32));
        else $response->cookie(Cookie::forget('gkauth_token'));

        return $response;
    }
}
