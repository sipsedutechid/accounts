<?php

// Landing
Route::get('/', function () {
	return redirect('/my');
});

Route::get('/account/verify/{token}', 'AccountController@getDoVerify');						// Verify user account
Route::post('/billing/vt/callback', 'BillingController@vtNotification');					// Veritrans callback

// CSRF protection, sessions, cookies, etc..
Route::group(['middlewareGroups' => ['web']], function ()
{
	// Authentication / Registration
	Route::get('/recover', 'Auth\PasswordController@getEmail');								// Forgot password form
	Route::get('/login', 'Auth\AuthController@showLoginForm');								// Login form
	Route::get('/logout', 'Auth\AuthController@logout');									// Logout action
	Route::get('/oauth/{provider}', 'Auth\AuthController@redirectToProvider');				// 3rd-party OAuth redirect
	Route::get('/oauth/callback/{provider}', 'Auth\AuthController@handleProviderCallback');	// 3rd-party OAuth callback
	Route::get('/register', 'Auth\AuthController@getRegister');								// New user registration form
	Route::get('/reset/{token}', 'Auth\PasswordController@getReset');						// Forgot password reset form
	Route::post('/recover', 'Auth\PasswordController@postEmail');							// Forgot password action
	Route::post('/register', 'Auth\AuthController@postRegister');							// New user registration action
	Route::post('/reset', 'Auth\PasswordController@postReset');								// Forgot password reset action

	Route::group(['middleware' => ['cors']], function ()
	{
		Route::post('/login', 'Auth\AuthController@postLogin');								// Login action
	});

	// Authenticated area
	Route::group(['middleware' => ['auth']], function ()
	{
	    Route::get('/my', 'AccountController@getHome');										// Profile home
	    Route::get('/my/invoices/{orderNo}', 'BillingController@getInvoice');				// View printable invoice
	    Route::get('/my/settings', 'AccountController@getSettings');						// View general settings form
	    Route::get('/my/settings/accounts', 'AccountController@getConnectedAccounts');		// List all connected accounts
	    Route::get('/my/settings/billing', 'AccountController@getBilling');					// View billing data form
		Route::get('/my/settings/password', 'AccountController@getPassword');				// View password change form
		Route::get('/my/subscriptions', 'SubscriptionController@getList');					// List all subscriptions
	    Route::post('/my/settings', 'AccountController@postSettings');						// general settings form action
	    Route::post('/my/settings/billing', 'AccountController@postBilling');				// Post billing data action
	    Route::post('/my/settings/password', 'AccountController@postPassword');				// Password change form action
		
	    Route::get('/account/verify', 'AccountController@getVerify');						// Initiate user account verification
	    Route::get('/order/confirm', 'BillingController@getOrderConfirm');					// View payment confirmation form
		Route::get('/subscribe', 'SubscriptionController@getSubscribe');					// Subscription checkout page
	    Route::post('/order/confirm', 'BillingController@postOrderConfirm');				// Payment confirmation form action
		Route::post('/subscribe', 'BillingController@postSubscribe');						// Initiate subscription order
		
		// Admin routes
		Route::group(['prefix' => 'orders'], function ()
		{
			Route::get('/all', 'Admin\OrderController@getList');							// List all subscription
			Route::get('/confirmations/all', 'Admin\OrderController@getConfirmationsList');	// List all payment confirmation
			Route::get('/invoices/{orderNo}', 'BillingController@getInvoice');				// View printable invoice
			Route::get('/{orderNo}', 'Admin\OrderController@getSingle');					// View single order
			Route::post('/action/{action}', 'Admin\OrderController@postAction');			// Subscription order action
			Route::delete('/confirmations', 'Admin\OrderController@deleteConfirmation');	// Delete a single payment confirmation
		});
		Route::group(['prefix' => 'users'], function ()
		{
			Route::get('/all', 'Admin\UserController@getList');								// List all registered accounts
			Route::get('/groups/all', 'Admin\UserGroupController@getList');					// List all account groups
			Route::get('/groups/create', 'Admin\UserGroupController@getCreate');			// New account group form
			Route::get('/groups/{name}', 'Admin\UserGroupController@getSingle');			// View single account group
			Route::get('/groups/{name}/subscribe', 'Admin\UserGroupController@getSubscribe');	// Create subscription for group form
			Route::get('/register', 'Admin\UserController@getCreate');						// New account registration form
			Route::get('/{email}', 'Admin\UserController@getEdit');							// View and edit existing account
			Route::get('/{email}/subscribe', 'Admin\UserController@getSubscribe');			// Create subscription for account form
			Route::post('/deactivate', 'Admin\UserController@getDeactivate');				// Deactivate account action
			Route::post('/edit', 'Admin\UserController@postEdit');							// Edit existing account action
			Route::post('/register', 'Admin\UserController@postCreate');					// New account registration action
			Route::post('/subscribe', 'Admin\UserController@postSubscribe');				// Create subscription for account action
			Route::post('/groups/subscribe', 'Admin\UserGroupController@postSubscribe');	// Create subscription for group members action
			Route::delete('/groups/{name}', 'Admin\UserGroupController@deleteSingle');		// Delete a single account group
		});
		Route::group(['prefix' => 'roles'], function ()
		{
			Route::get('/all', 'Admin\RoleController@getList');								// List all application roles
			Route::get('/create', 'Admin\RoleController@getCreate');						// New application role form
			Route::get('/{roleId}', 'Admin\RoleController@getEdit');						// Edit existing role
			Route::post('/create', 'Admin\RoleController@postCreate');						// New application role action
			Route::post('/{roleId}', 'Admin\RoleController@postEdit');						// Edit existing role action
		});
	});

	// API
	Route::group(['prefix' => 'api', 'middleware' => ['api', 'cors']], function ()
	{
		Route::get('/status', 'API\AccountController@getStatus');
		Route::get('/available-topics', 'API\AccountController@getTopics');
		Route::post('/subscribe-email', 'API\EmailSubscriptionController@postSubscribe');

		Route::group(['middleware' => 'auth'], function ()
		{
			Route::get('/users/search', 'API\UserController@getSearch');
			Route::get('/users/user-group/{group}', 'API\UserController@getListByUserGroup');
			Route::post('/users/user-group/{group}/{action}', 'API\UserController@postUserGroupAction');
			Route::post('/user-groups/save', 'API\UserGroupController@postSave');
		});
	});
	
});

// Testing
Route::group(['prefix' => 'test'], function ()
{
	Route::get('/curl', 'TestController@curl');
	Route::get('/email/{view}', 'TestController@getEmail');
	Route::get('/email', function ()
	{
		if (env('TEST_EMAIL', null) == null)
			return abort(403);
		Mail::queue('emails.id.welcome', ['user' => \App\User::find(1)], function ($m)
        {
            $m->from('support@gakken-idn.co.id', 'Gakken Support');
            $m->to(env('TEST_EMAIL', "endy.hardy@gakken-idn.co.id"), "Endy Hardy")->subject("This is a Test Message via Mailgun Sandbox");
        });

        return "Email sent";
	});
});