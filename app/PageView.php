<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageView extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = null;
    protected $fillable = ['user_id', 'url', 'accessed_at'];
    protected $dates = ['accessed_at'];

    public function user()
    {
    	return $this->belongsTo('\App\User');
    }
}
