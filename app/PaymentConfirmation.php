<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentConfirmation extends Model
{
	protected $appends = ['created_at_label', 'transfer_date_label'];

	public function getCreatedAtLabelAttribute()
	{
        return date(date('Ymd', strtotime($this->created_at)) == date('Ymd') ? 'g:i a' : 'j M y', strtotime($this->created_at));
	}

	public function getTransferDateLabelAttribute()
	{
        return date('j-m-Y', strtotime($this->transfer_date));
	}
}
