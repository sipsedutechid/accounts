<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	// Relations

    public function abilities()
    {
        return $this->belongsToMany('App\Ability', 'role_abilities');
    }
    public function users()
    {
    	return $this->hasMany('App\User');
    }

    // Field modifiers

    public function getUpdatedAtAttribute($value)
    {
        return date(date('Ymd', strtotime($value)) == date('Ymd') ? 'g:i a' : 'j M y', strtotime($value));
    }

    // Custom methods

    public static function dropdown()
    {
    	$roles = [];
    	foreach(Role::all() as $role) {
    		$roles[$role->id] = $role->name;
    	}
    	return $roles;
    }
}
