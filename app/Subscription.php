<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ['name', 'label', 'duration', 'period', 'price', 'description', 'is_custom'];

}
