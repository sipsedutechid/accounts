<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionOrder extends Model
{
	use SoftDeletes;
    
	protected $dates = ['expired_at', 'settled_at', 'subscribed_at'];

	// Relations

	public function subscription()
	{
		return $this->belongsTo('App\Subscription');
	}
	public function user()
	{
		return $this->belongsTo('App\User');
	}
	public function approvedBy()
	{
		return $this->belongsTo('App\User', 'approved_by');
	}

	// Field modifiers

	public function getCreatedAtAttribute($value)
	{
        return date(date('Ymd', strtotime($value)) == date('Ymd') ? 'g:i a' : 'j M y', strtotime($value));
	}

    // Custom methods

	public function approve()
	{
		if ($this->settled_at)
			return $this;
		$this->approved_by = auth()->user()->id;
		$this->settled_at = time();
		$this->status = "settled";
		$this->save();

		return $this->intoSubscription();
	}
	public function deny()
	{
		// TODO
	}
	public static function generateOrderNumber()
	{
		$count = SubscriptionOrder::whereDate('created_at', '=', date('Y-m-d'))->withTrashed()->count();
		return 'GK-' . date('ymd') . str_pad($count + 1, 4, '0', STR_PAD_LEFT);
	}
	public function intoSubscription()
	{
		if ($this->subscribed_at)
			return $this;

		$currentSubscription = $this
			->user
			->userSubscriptions()
			->where('expired_at', '>', time())
			->first();
		$newStart = $currentSubscription ? strtotime($currentSubscription->expired_at) : time();

		$newSubscription = new \App\UserSubscription([
			'subscription_id'	=> $this->subscription->id,
			'starts_at' 		=> $newStart,
			'expired_at'		=> strtotime('+' . $this->quantity . ' ' . $this->subscription->period, $newStart),
			]);
		$this->user->userSubscriptions()->save($newSubscription);
		$newSubscription->intoTopics();

		$this->expired_at = null;
		$this->subscribed_at = time();

		return $this->save();
	}
	public function pend()
	{
		// TODO
	}
	public function settle()
	{
		if ($this->settled_at)
			return $this;
		$this->settled_at = time();
		$this->status = "settled";
		return $this->save();
	}

}
