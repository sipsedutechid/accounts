<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $connection = 'mysql_vault';
    protected $appends = ['created_at_label'];
    protected $with = ['createdBy', 'lectures'];
    protected $hidden = ['summary', 'created_by', 'created_at', 'updated_at'];

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function lectures() {
        return $this->belongsToMany('App\Lecture', 'topic_lecture');
    }

    public function getCreatedAtLabelAttribute($value)
    {
        return date('Ymd', strtotime($value)) == date('Ymd') ? date('H:i', strtotime($value)) : date('j M y', strtotime($value));
    }

}
