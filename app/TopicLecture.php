<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicLecture extends Model
{
    protected $connection = 'mysql_vault';
    protected $table    = 'topic_lecture';
    public $timestamps  = false;
}
