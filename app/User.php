<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $dates = ['verify_expired_at', 'last_active_at', 'verified_at', 'deactivated_at'];
    protected $fillable = ['name', 'email', 'password', 'role_id'];
    protected $hidden = ['id', 'password', 'remember_token', 'is_provider', 'role_id', 'verify_token', 'verify_expired_at', 'last_active_at', 'verified_at', 'deactivated_at', 'created_at', 'updated_at'];
    protected $with = ['userDetail'];

    // Relations

    public function connectedAccounts()
    {
        return $this->hasMany('App\ConnectedAccount');
    }
    public function role()
    {
        return $this->belongsTo('App\Role');
    }
    public function subscriptionOrders()
    {
        return $this->hasMany('App\SubscriptionOrder');
    }
    public function topics()
    {
        return $this->belongsToMany('App\Topic', 'user_topics');
    }
    public function userGroups()
    {
        return $this->belongsToMany('App\UserGroup', 'user_group_members')->withTimestamps();
    }
    public function userTopics()
    {
        return $this->hasMany('App\UserTopic');
    }
    public function userDetail()
    {
        return $this->hasOne('App\UserDetail');
    }
    public function userSubscriptions()
    {
        return $this->hasMany('App\UserSubscription');
    }

    // Field modifiers

    public function getCreatedAtAttribute($value)
    {
        return date(date('Ymd', strtotime($value)) == date('Ymd') ? 'g:i a' : 'j M y', strtotime($value));
    }
    public function getNameAttribute($value)
    {
        if (strpos($value, "("))
            return trim(substr($value, 0, strpos($value, "(")));
        return trim($value);
    }

    // Custom functions

    public function clearVerify()
    {
        $this->verify_token = null;
        $this->verify_expired_at = null;
        return $this->save();
    }
    public function currentSubscription()
    {
        return $this->userSubscriptions()
            ->where('expired_at', '>', \Carbon\Carbon::now())
            ->where('starts_at', '<', \Carbon\Carbon::now())
            ->orderBy('expired_at')
            ->first();
    }
    public function getStatus()
    {
        if ($this->deactivated_at) return "deactivated";
        if ($this->verified_at) return "verified";
        return "unverified";
    }
    public function initVerify()
    {
        $token = str_random(128);
        $this->verify_token = $token;
        $this->verify_expired_at = strtotime("+1 day");
        $this->save();

        return $token;
    }
    public function isSubscriber()
    {
        return $this->role->is_subscriber;
    }
    public function isUser()
    {
        return $this->role->is_user;
    }
    public function refresh()
    {
        $this->last_active_at = time();
        return $this->save();
    }
    public function verify()
    {
        $this->verified_at = time();
        $this->save();
        return $this->clearVerify();
    }
    public function toMoodleUser()
    {
        $apiUrl = env('LMS_API_URL', null);
        if ($apiUrl == null)
            return 'No API URL defined';
        $student = [
            "username" => $this->email,
            "password" => str_random(16),
            "firstname" => $this->name,
            "lastname" => $this->name,
            "email" => $this->email,
            "auth" => "gakken",
            "idnumber" => 'GKSSO-' . str_pad($this->id, 8, '0', STR_PAD_LEFT),
            "lang" => "id",
            "calendartype" => "",
            "theme" => "",
            "timezone" => "Asia/Makassar",
            "mailformat" => 0,
            "description" => "",
            "city" => "",
            "country" => "",
            "firstnamephonetic" => "",
            "lastnamephonetic" => "",
            "middlename" => "",
            "alternatename" => ""
        ];
        $response = \Ixudra\Curl\Facades\Curl::to($apiUrl)
            ->withData([
                'wstoken' => env('LMS_API_TOKEN', ''),
                'wsfunction' => 'core_user_create_users',
                'moodlewsrestformat' => 'json',
                'users' => [$student]
                ])
            ->post();
        return $response;
    }
}
