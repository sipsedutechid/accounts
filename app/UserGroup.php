<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserGroup extends Model
{
	use SoftDeletes;

    // Relations

    public function adminUser()
    {
    	return $this->belongsTo('App\User', 'admin_id');
    }
    public function members()
    {
    	return $this->belongsToMany('App\User', 'user_group_members')->withTimestamps();
    }

    // Field modifiers

    public function getCreatedAtAttribute($value)
    {
        return date(date('Ymd', strtotime($value)) == date('Ymd') ? 'g:i a' : 'j M y', strtotime($value));
    }
}
