<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $dates = ['starts_at', 'expired_at'];
    protected $fillable = ['subscription_id', 'starts_at', 'expired_at'];
    protected $hidden = ['id', 'user_id', 'subscription_id', 'created_at', 'updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function subscription()
    {
    	return $this->belongsTo('App\Subscription');
    }

    public function intoTopics()
    {
        $user = $this->user;
        $current = \App\AppSetting::find('topic.current_index_' . $user->profession)->value;
        $topicIndices = [];
        $backnumberIndices = [];

        if (!$current)
            return null;
        for ($i = $current; $i < $current + 11; $i++) { 
            array_push($topicIndices, $i);
        }
        $topics['accessible'] = \App\Topic::where('category', $user->profession)
            ->whereIn('release', $topicIndices)
            ->get();
        foreach ($topics['accessible'] as $topic) {
            $userTopic = new \App\UserTopic;
            $userTopic->user_id = $user->id;
            $userTopic->topic_id = $topic->id;
            $userTopic->is_accessible = true;
            $userTopic->is_backnumber = false;
            $userTopic->save();

            // Enrol if P2KB
        }

        for ($i = $current - 1; $i < $current - 12; $i--) { 
            if ($i > 0) array_push($backnumberIndices, $i);
        }
        $topics['backnumbers'] = \App\Topic::where('category', $user->profession)
            ->whereIn('release', $backnumberIndices)
            ->get();
        foreach ($topics['accessible'] as $topic) {
            $userTopic = new \App\UserTopic;
            $userTopic->user_id = $user->id;
            $userTopic->topic_id = $topic->id;
            $userTopic->is_accessible = false;
            $userTopic->is_backnumber = true;
            $userTopic->save();
        }
        return $this;
    }
}
