<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTopic extends Model
{
	public $timestamps = false;
	// protected $appends = ['topic'];

    // Relations
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function topic()
    {
    	return $this->belongsTo('App\Topic')->select([
            'id',
            'title',
            'slug',
            'is_p2kb',
            'summary',
            'category',
            'publish_at',
            'featured_image',
            'release',
            'post_status'
            ]);
    }
   //  public function getTopicAttribute()
   //  {
   //      return $this->topic()->select([
   //          'title',
   //          'slug',
   //          'is_p2kb',
   //          'summary',
   //          'featured_image',
			// 'release',
   //          ])
			// ->first();
   //  }

}
