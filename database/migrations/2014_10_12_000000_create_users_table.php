<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');                           // User ID
            $table->string('name');                             // Real name
            $table->string('email')->unique();                  // Registered email
            $table->string('password');                         // Hashed password
            $table->string('profession')->nullable();           // Profession
            $table->string('id_no')->nullable();                // Professional ID Number
            $table->string('avatar_url')->nullable();           // Avatar URL
            $table->rememberToken();
            $table->string('verify_token');                     // Verification token
            $table->boolean('is_provider')->default(false);     // Whether the account is provider-verified
            $table->integer('role_id')->unsigned();             // User role
            $table->timestamp('verify_expired_at')->nullable(); // Time when verification token expires
            $table->timestamp('last_active_at')->nullable();    // Last access
            $table->timestamp('verified_at')->nullable();       // Verification time
            $table->timestamp('deactivated_at')->nullable();    // Deactivation time
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
