<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Subscription;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('label');
            $table->boolean('is_custom')->default(false);
            $table->integer('duration');
            $table->string('period');
            $table->text('description')->nullable();
            $table->bigInteger('price')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Subscription::create([
            'name'      => 'custom', 
            'label'     => 'Custom Subscription', 
            'duration'  => 1, 
            'period'    => 'days', 
            'price'     => 0, 
            'is_custom' => true
            ]);
        Subscription::create([
            'name'      => 'yearly', 
            'label'     => 'Gakken Yearly Subscription', 
            'duration'  => 1, 
            'period'    => 'years', 
            'price'     => 8000000
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscriptions');
    }
}
