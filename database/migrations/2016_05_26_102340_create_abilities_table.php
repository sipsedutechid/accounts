<?php

use App\Ability;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abilities', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
        });

        // Account abilities
        Ability::create(['name' => 'accounts.users.view']);
        Ability::create(['name' => 'accounts.users.create']);
        Ability::create(['name' => 'accounts.users.create_super']);
        Ability::create(['name' => 'accounts.users.edit']);
        Ability::create(['name' => 'accounts.users.deactivate']);
        Ability::create(['name' => 'accounts.users.subscription.subscribe']);
        Ability::create(['name' => 'accounts.users.orders.view']);
        Ability::create(['name' => 'accounts.users.orders.create']);
        Ability::create(['name' => 'accounts.users.orders.edit']);
        Ability::create(['name' => 'accounts.roles.view']);
        Ability::create(['name' => 'accounts.roles.create']);
        Ability::create(['name' => 'accounts.roles.edit']);
        Ability::create(['name' => 'accounts.roles.delete']);

        // Vault abilities
        Ability::create(['name' => 'vault.drugs.view']);
        Ability::create(['name' => 'vault.drugs.create']);
        Ability::create(['name' => 'vault.drugs.edit']);
        Ability::create(['name' => 'vault.drugs.delete']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('abilities');
    }
}
