<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_confirmations', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('order_no');
            $table->string('bank');
            $table->string('account_no');
            $table->string('account_name');
            $table->string('to_account');
            $table->string('ref_no');
            $table->date('transfer_date');
            $table->integer('amount')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_confirmations');
    }
}
