<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTopicsTableAddIsPassedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_topics', function($table) {
            $table->increments('id')->first();
            $table->float('grade')->default(false);
            $table->timestamp('passed_at')->nullable();
            $table->string('certificate_no')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_topics', function($table) {
            $table->dropColumn('id');
            $table->dropColumn('grade');
            $table->dropColumn('passed_at');
            $table->dropColumn('certificate_no');
        });
    }
}
