<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableAddNameOnCertificateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('name_on_certificate')->after('id_no')->nullable();
        });

        foreach (\App\User::all() as $user) {
            $user->name_on_certificate = substr($user->name, 0, 50);
            $user->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('name_on_certificate');
        });
    }
}
