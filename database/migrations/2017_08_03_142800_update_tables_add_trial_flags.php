<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablesAddTrialFlags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function($table) {
            $table->boolean('is_trial')->after('is_custom')->default(false);
            $table->string('redeem_code')->after('price')->nullable();
        });
        Schema::table('user_subscriptions', function($table) {
            $table->boolean('is_trial')->after('subscription_id')->default(false);
        });
        Schema::table('user_topics', function($table) {
            $table->boolean('is_trial')->after('is_backnumber')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function($table) {
            $table->dropColumn('is_trial');
            $table->dropColumn('redeem_code');
        });
        Schema::table('user_topics', function($table) {
            $table->dropColumn('is_trial');
        });
        Schema::table('user_subscriptions', function($table) {
            $table->dropColumn('is_trial');
        });
    }
}
