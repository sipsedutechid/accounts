<?php

use App\AppSetting;
use Illuminate\Database\Seeder;

class AppSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AppSetting::create(['key' => 'company.name', 'value' => "PT. GAKKEN HEALTH AND EDUCATION INDONESIA"]);
        AppSetting::create(['key' => 'company.address_1', 'value' => "Kompleks Ruko Ratulangi"]);
        AppSetting::create(['key' => 'company.address_2', 'value' => "Jl. Dr. Sam Ratulangi No. 7/C9"]);
        AppSetting::create(['key' => 'company.city', 'value' => "Makassar"]);
        AppSetting::create(['key' => 'company.province', 'value' => "South Sulawesi"]);
        AppSetting::create(['key' => 'company.country', 'value' => "Indonesia"]);
        AppSetting::create(['key' => 'company.postal_code', 'value' => "90133"]);
        AppSetting::create(['key' => 'company.phone', 'value' => "0411 8111 255"]);
        AppSetting::create(['key' => 'company.fax', 'value' => "0411 8111 218"]);
        AppSetting::create(['key' => 'company.email', 'value' => "contact@gakken-idn.co.id"]);
        AppSetting::create(['key' => 'checkout.banktransfer.instructions', 'value' => "PT. GAKKEN HEALTH AND EDUCATION INDONESIA\nAccount No: 1133113381\nBank: BANK NEGARA INDONESIA (BNI46)\nBranch: Makassar\nBank Code: 008"]);
    }
}
