<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AppSettingsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(OrderAbilitySeeder::class);
        $this->call(UserGroupAbilitySeeder::class);
        $this->call(PaymentConfirmationAbilitySeeder::class);
    }
}
