<?php

use App\Ability;
use App\Role;
use Illuminate\Database\Seeder;

class OrderAbilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$abilities = [
    		Ability::create(['name' => 'accounts.orders.view']),
    		Ability::create(['name' => 'accounts.orders.create']),
    		Ability::create(['name' => 'accounts.orders.delete']),
            Ability::create(['name' => 'accounts.orders.approve']),
    		Ability::create(['name' => 'accounts.orders.cancel']),
    	];

        Role::where('is_super', true)->first()->abilities()->saveMany($abilities);
    }
}
