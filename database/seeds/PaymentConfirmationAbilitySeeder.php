<?php

use Illuminate\Database\Seeder;
use App\Ability;
use App\Role;

class PaymentConfirmationAbilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$abilities = [
    		Ability::create(['name' => 'accounts.payment_confirmations.view']),
    		Ability::create(['name' => 'accounts.payment_confirmations.delete']),
    	];

        Role::where('is_super', true)->first()->abilities()->saveMany($abilities);
    }
}
