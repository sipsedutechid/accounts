<?php

use App\Ability;
use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superRole = Role::create(['name' => 'Super User', 'is_super' => true]);
        $adminRole = Role::create(['name' => 'App Administrator']);
        Role::create(['name' => 'User', 'is_user' => true]);
        Role::create(['name' => 'Subscriber', 'is_subscriber' => true]);

        $superRole->abilities()->saveMany(Ability::all());
        $adminRole->abilities()->saveMany(Ability::where('name', '<>', 'accounts.users.create_super')->get());
    }
}
