<?php

use Illuminate\Database\Seeder;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $current = strtotime('-12 month');
        $limit = strtotime('+12 month');
        $release_year_months = [];
        while ($current < $limit) {
        	array_push($release_year_months, date('Ym', $current));
        	$current = strtotime('+1 month', $current);
        }

        foreach ($release_year_months as $year_month) {
        	$topic = factory(App\Topic::class)->make();
        	$topic->release_year_month = $year_month;
        	$topic->save();
        }
    }
}
