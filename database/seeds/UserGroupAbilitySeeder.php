<?php

use Illuminate\Database\Seeder;
use App\Ability;
use App\Role;

class UserGroupAbilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Role::where('is_super', true)->first()->abilities()->saveMany([
	        Ability::create(['name' => 'accounts.user_groups.view']),
	        Ability::create(['name' => 'accounts.user_groups.create']),
	        Ability::create(['name' => 'accounts.user_groups.delete']),
	        Ability::create(['name' => 'accounts.user_groups.manage']),
	       ]);
    }
}
