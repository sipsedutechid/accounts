<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	"name"	    => "Gakken Indonesia",
        	"email"	    => "test@example.com",
        	"password"	=> bcrypt("12345678"),
            "role_id"   => Role::where('is_super', true)->first()->id
        	]);
        $user->verify();
    }
}
