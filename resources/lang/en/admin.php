<?php

return [
	'orders'		=> [
		'action'	=> [
			'back_list'	=> 'Back to Orders List',
		],
		'confirmations'	=> [
			'created_at'	=> 'Submitted at',
			'delete'		=> 'Delete',
			'empty'			=> 'No payment confirmations found.',
			'order_no'		=> 'Order No.',
			'title_list'	=> 'Payment Confirmations',
			'from_account'	=> 'Transfer from',
			'to_account'	=> 'Transfer to',
		],
		'form'		=> [
			'title_new'	=> 'Create Order',
		],
		'single'	=> [
			'title'	=> 'Order: :order_no',
		],
		'list'		=> [
			'empty'	=> 'No subscription orders found',
			'title'	=> 'All Orders',
		],
		'messages'	=> [
			'approved'		=> 'Order approved.',
			'cannot_delete'	=> 'The subscription order cannot be deleted',
			'deleted'		=> 'Order deleted.',
		],
		'title'	=> 'Order Management',
	],
	'roles'			=> [
		'action'	=> [
			'back_list'	=> 'Back to Roles List',
			'create'	=> 'Create New Role',
		],
		'form'		=> [
			'fields'		=> [
				'abilities'	=> 'Permissions',
				'name'		=> 'Role Name',
			],
			'submit_edit'	=> 'Save Changes',
			'submit_new'	=> 'Create Role',
			'title_edit'	=> 'Edit Role',
			'title_new'		=> 'New Role',
		],
		'list'		=> [
			'title'	=> 'All App Roles',
		],
		'messages'	=> [
			'success_edit'	=> 'Role saved.',
			'success_new'	=> 'Role created.',
		],
	],
	'title_menu'	=> 'Administrative Tasks',
	'users'			=> [
		'action'	=> [
			'back_details'	=> 'Back to Account Details',
			'back_list'		=> 'Back to Accounts List',
		],
		'groups'	=> [
			'action'		=> [
				'back_list'	=> 'Back to Account Groups List',
			],
			'list'			=> [
				'empty'	=> 'No account groups found.',
				'title'	=> 'All Account Groups',
			],
			'members_count'	=> ":count registered member|:count registered members",
			'messages'		=> [
				'delete_success'	=> 'User group deleted',
			],
			'title_new'		=> 'Create Group'
		],
		'list'		=> [
			'created_at'	=> 'Registered',
			'email'			=> 'Email',
			'empty'			=> 'No accounts found',
			'groups'		=> 'Groups',
			'name'			=> 'Full Name',
			'search'		=> 'Search accounts..',
			'status'		=> 'Status',
			'title'			=> 'All Accounts',
		],
		'register'	=> [
			'action'			=> [
				'subscribe'	=> 'Assign subscription',
			],
			'prof_details'		=> 'Professional Details',
			'billing_details'	=> 'Billing Details',
			'fields'			=> [
				'autoverify'			=> 'Automatically verify this account',
				'email'					=> 'Email Address',
				'name'					=> 'Full Name',
				'password_new'			=> 'New Password',
				'password_change'		=> 'Change Password',
				'password_confirmation'	=> 'Verify Password',
				'role'					=> 'App Role',
				'send_welcome_email'	=> 'Send welcome email',
				'submit_new'			=> 'Create Account',
				'submit_change'			=> 'Save Changes',
			],
			'messages'			=> [
				'success_edit'	=> 'User saved.',
				'success_new'	=> 'User registered.',
			],
			'not_subscribed'	=> 'This user has no active subscription.',
			'orders_empty'		=> 'This user has not ordered any subscriptions.',
			'subscription'		=> 'Subscription Status',
			'title_edit'		=> 'Account Details',
			'title_new'			=> 'Register New Account',
		],
		'subscribe'	=> [
			'account'			=> 'Account',
			'date_start'		=> 'Start Date',
			'date_end'			=> 'Expiry Date',
			'duration'			=> 'Duration',
			'duration_days'		=> 'Days',
			'duration_weeks'	=> 'Weeks',
			'duration_months'	=> 'Months',
			'duration_years'	=> 'Years',
			'fixed_dates'		=> 'Set fixed dates',
			'fixed_dates_help'	=> 'If unchecked, this subscription will be appended to the end of the current subscription.',
			'messages'			=> [
				'success_new'	=> 'Account has been subscribed.',
			],
			'save'				=> 'Subscribe Account',
			'title'				=> 'Assign Subscription',
		],
		'title'		=> 'Account Management',
	],
];