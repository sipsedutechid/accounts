<?php

return [
	'about'					=> 'About',
	'action'				=> [
		'back'		=> 'Go Back',
		'change'	=> 'Ubah',
		'done'		=> 'Done',
	],
	'dropdown_placeholder'	=> 'Please select',
	'help'					=> 'Account Help',
	'page_title'			=> ':title | Gakken',
	'pagination'			=> 'Showing :first to :last of :total',
];