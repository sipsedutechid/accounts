<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'action'    => [
        'forgot'    => 'Forgot Password?',
        'login'     => 'Sign In',
        'logout'    => 'Sign Out',
        'register'	=> 'Sign Up',
    ],
    'failed' 	=> "Oops! We couldn't find an account using those credentials.",
    'forgot'    => [
        'email'     => 'Email Address',
        'help'      => 'Please provide your email address in the box below and we will send you password reset link.',
        'submit'    => 'Send Me a Reset Link',
        'title'     => 'Recover Account',
    ],
    'login'		=> [
    	'email'			    => 'Email Address',
        'facebook'          => 'Sign in with Facebook',
        'google'            => 'Sign in with Google',
    	'page_title'	    => 'Sign In',
        'password'          => 'Password',
        'remember_me'       => 'Remember Me',
        'remember_me_help'  => '(uncheck on a shared device)',
        'submit'            => 'Sign In',
        'title'             => 'Sign In to Your Account',
    ],
    'register'	=> [
    	'field'			=> [
    		'agreement'			=> 'I agree that I have read the agreement bla bla bla..',
    		'email'				=> 'Email Address',
    		'name'				=> 'Full Name',
    		'password'			=> 'Set a New Password',
    		'password_confirm'	=> 'Verify Your Password'
    	],
        'to_login'      => "Sign in instead",
    	'login_help'	=> "Don't have an account?",
    	'submit'		=> 'Create My Account',
    	'title'			=> 'Create an Account',
    ],
    'throttle' 	=> 'Too many login attempts. Please try again in :seconds seconds.',

];
