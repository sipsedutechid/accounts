<?php

return [
	'action'	=> [
		'checkout'		=> 'Place Order',
		'checkout_vt'	=> 'Continue to Payment',
		'choose'		=> 'Choose this',
		'subscribe'		=> 'Subscribe',
	],
	'has_subscription'	=> 'You are currently subscribed for Gakken services until :expire',
	'no_description'	=> 'No description set',
	'order'		=> [
		'confirm'	=> [
			'amount'			=> 'Amount of Transfer',
			'back_orders'		=> 'Back to Order History',
			'bank'				=> 'Bank Name',
			'help'				=> 'Please fill out the following form to confirm your payment',
			'from_account'		=> 'From Your Account',
			'from_account_no'	=> 'Account No.',
			'from_account_name'	=> 'Account Name',
			'order_no'			=> 'For Order No.',
			'order_no_help'		=> 'You may check your pending Order No. in your',
			'ref_no'			=> 'Reference No. (optional)',
			'save'				=> 'Submit',
			'title'				=> 'Confirm Payment',
			'to_account'		=> 'To Gakken Account',
			'to_account_choice'	=> 'Account',
			'transfer_date'		=> 'Date of Transfer'
		],
		'status'	=> [
			'approved'	=> 'Approved',
			'pending'	=> 'Pending',
			'settled'	=> 'Settled',
			'success'	=> 'Payment Success',
		],
	],
	'self'		=> [
		'empty'					=> 'You have not purchased any subscriptions.',
		'no_billing_details'	=> 'You need to complete your billing details before subscribing.',
		'orders'				=> [
			'created_at'		=> 'Ordered On',
			'empty'				=> 'You have not ordered any subscriptions',
			'subscription'		=> 'Subscription',
			'order_no'			=> 'Order No.',
			'order_received'	=> 'Your order for :subscription has been received. Please make necessary payment within 24 hours.',
			'quantity'			=> 'Quantity',
			'status'			=> 'Status',
			'status_pending'	=> 'Pending',
			'status_success'	=> 'Payment Success',
			'title'				=> 'Order History',
			'total'				=> 'Total',
		],
		'title'					=> 'Subscriptions',
	],
	'subscribe'	=> [
		'options'				=> [
			'monthly'	=> 'Monthly',
			'yearly'	=> 'Yearly',
		],
		'payment_method'		=> [
			'banktransfer'		=> 'Bank Transfer',
			'title'				=> 'Payment Method',
			'vtweb'				=> 'Credit Card, Debit Card',
			'vtwebinstallment'	=> 'Credit Card Installment',
		],
		'periods' 				=> [
			'm'	=> 'month(s)',
			'y'	=> 'year(s)'
		],
		'title_new'				=> 'Choose a subscription',
		'title_change'			=> 'Upgrade your subscription',
		'veritrans_disclaimer'	=> 'You will be redirected to the Midtrans payment page'
	]
];