<?php

return [
	'orders'		=> [
		'action'	=> [
			'back_list'	=> 'Kembali ke Daftar Pesanan',
		],
		'confirmations'	=> [
			'created_at'	=> 'Dibuat pada',
			'delete'		=> 'Hapus',
			'empty'			=> 'Tidak ada konfirmasi pembayaran yang ditemukan.',
			'order_no'		=> 'No. Pesanan',
			'title_list'	=> 'Konfirmasi Pembayaran',
			'from_account'	=> 'Transfer dari',
			'to_account'	=> 'Transfer ke',
		],
		'form'		=> [
			'title_new'	=> 'Buat Pesanan',
		],
		'single'	=> [
			'title'	=> 'Pesanan: :order_no',
		],
		'list'		=> [
			'empty'	=> 'Tidak ada pesanan yang ditemukan',
			'title'	=> 'Semua Pesanan',
		],
		'messages'	=> [
			'approved'		=> 'Pesanan disetujui.',
			'cannot_delete'	=> 'Pesanan tidak dapat dihapus',
			'deleted'		=> 'Pesanan dihapus.',
		],
		'title'	=> 'Manajemen Pesanan',
	],
	'roles'			=> [
		'action'	=> [
			'back_list'	=> 'Kembali ke Daftar Peran',
			'create'	=> 'Buat Peran Baru',
		],
		'form'		=> [
			'fields'		=> [
				'abilities'	=> 'Izin',
				'name'		=> 'Nama Peran',
			],
			'submit_edit'	=> 'Simpan',
			'submit_new'	=> 'Simpan',
			'title_edit'	=> 'Ubah Peran',
			'title_new'		=> 'Buat Peran',
		],
		'list'		=> [
			'title'	=> 'Semua Peran',
		],
		'messages'	=> [
			'success_edit'	=> 'Peran tersimpan.',
			'success_new'	=> 'Peran baru tersimpan.',
		],
	],
	'title_menu'	=> 'Menu Administrator',
	'users'			=> [
		'action'	=> [
			'back_details'	=> 'Kembali ke Rincian Akun',
			'back_list'		=> 'Kembali ke Daftar Akun',
		],
		'groups'	=> [
			'action'		=> [
				'back_list'	=> 'Kembali ke Daftar Grup Akun',
			],
			'list'			=> [
				'empty'	=> 'Tidak ada grup akun yang ditemukan.',
				'title'	=> 'Semua Grup Akun',
			],
			'messages'		=> [
				'create_success'	=> 'Grup akun berhasil dibuat.',
				'delete_success'	=> 'Grup akun dihapus.',
			],
			'members_count'	=> ":count akun terdaftar",
			'name'			=> 'Grup akun',
			'title_new'		=> 'Buat Grup'
		],
		'list'		=> [
			'created_at'	=> 'Dibuat pada',
			'email'			=> 'Email',
			'empty'			=> 'Tidak ada akun yang ditemukan',
			'groups'		=> 'Grup',
			'name'			=> 'Nama Lengkap',
			'search'		=> 'Cari akun..',
			'status'		=> 'Status',
			'title'			=> 'Semua Akun',
		],
		'register'	=> [
			'action'			=> [
				'subscribe'	=> 'Berikan Langganan',
			],
			'prof_details'		=> 'Rincian Profesi',
			'billing_details'	=> 'Rincian Penagihan',
			'fields'			=> [
				'autoverify'			=> 'Verifikasi akun ini',
				'email'					=> 'Email',
				'name'					=> 'Nama Lengkap',
				'password_new'			=> 'Kata Sandi Baru',
				'password_change'		=> 'Ubah Kata Sandi',
				'password_confirmation'	=> 'Verifikasi Kata Sandi',
				'role'					=> 'Peran',
				'send_welcome_email'	=> 'Kirim email selamat datang',
				'submit_new'			=> 'Buat Akun',
				'submit_change'			=> 'Simpan',
			],
			'messages'			=> [
				'success_edit'	=> 'Akun disimpan.',
				'success_new'	=> 'Akun berhasil didaftar.',
			],
			'not_subscribed'	=> 'Akun ini tidak sedang berlangganan.',
			'orders_empty'		=> 'Akun ini belum memesan paket berlangganan apapun.',
			'subscription'		=> 'Status Berlangganan',
			'title_edit'		=> 'Rincian Akun',
			'title_new'			=> 'Buat Akun Baru',
		],
		'subscribe'	=> [
			'account'			=> 'Akun',
			'date_start'		=> 'Tanggal Mulai',
			'date_end'			=> 'Tanggal Berakhir',
			'duration'			=> 'Durasi',
			'duration_days'		=> 'Days',
			'duration_weeks'	=> 'Weeks',
			'duration_months'	=> 'Months',
			'duration_years'	=> 'Years',
			'fixed_dates'		=> 'Beri jangka waktu tetap',
			'fixed_dates_help'	=> 'Jika tidak dicentang, paket berlangganan akan memperpanjang paket berlangganan yang sudah ada.',
			'messages'			=> [
				'success_new'	=> 'Akun telah diberi langganan.',
			],
			'save'				=> 'Simpan',
			'title'				=> 'Beri Langganan',
		],
		'title'		=> 'Manajemen Akun',
	],
];