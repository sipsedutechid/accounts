<?php

return [
	'about'					=> 'Tentang',
	'action'				=> [
		'back'		=> 'Kembali',
		'change'	=> 'Ubah',
		'done'		=> 'Selesai',
	],
	'dropdown_placeholder'	=> 'Harap dipilih',
	'help'					=> 'Bantuan Akun',
	'page_title'			=> ':title | Gakken',
	'pagination'			=> 'Rekor :first ke :last dari :total',
];