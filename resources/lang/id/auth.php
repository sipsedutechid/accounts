<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'action'    => [
        'forgot'    => 'Lupa Kata Sandi?',
        'login'     => 'Masuk',
        'logout'    => 'Keluar',
        'register'	=> 'Daftar',
    ],
    'failed' 	=> "Maaf, kami tidak menemukan akun tersebut.",
    'forgot'    => [
        'email'     => 'Alamat Email',
        'help'      => 'Harap mengisi email Anda dibawah dan kami akan mengirimkan pesan reset kata sandi.',
        'submit'    => 'Kirim Tautan Reset',
        'title'     => 'Pulihkan Akun',
    ],
    'login'		=> [
    	'email'			    => 'Alamat Email',
        'facebook'          => 'Masuk dengan Facebook',
        'google'            => 'Masuk dengan Google',
    	'page_title'	    => 'Masuk',
        'password'          => 'Kata Sandi',
        'remember_me'       => 'Ingat saya',
        'remember_me_help'  => '(jangan dicentang bagi komputer umum)',
        'submit'            => 'Masuk',
        'title'             => 'Masuk ke Akun Anda',
    ],
    'register'	=> [
    	'field'			=> [
    		'agreement'			=> 'I have read the Terms and Conditions',
    		'email'				=> 'Alamat Email',
    		'name'				=> 'Nama Lengkap',
    		'password'			=> 'Buat Kata Sandi',
    		'password_confirm'	=> 'Verifikasi Kata Sandi'
    	],
        'to_login'      => "Saya sudah memiliki akun",
    	'login_help'	=> "Tidak memiliki akun?",
    	'submit'		=> 'Buat Akun Saya',
    	'title'			=> 'Daftar Akun Baru',
    ],
    'throttle' 	=> 'Terlalu banyak percobaan masuk. Harap coba lagi dalam :seconds detik.',

];
