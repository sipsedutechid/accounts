<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata sandi harus memiliki minimal 8 karakter.',
    'reset' => 'Kata sandi Anda telah diatur ulang!',
    'reset_subject' => 'Pulihkan Akun Gakken Anda',
    'sent' => 'Tautan reset kata sandi telah dikirimkan',
    'token' => 'Token reset kata sandi ini tidak valid.',
    'user' => "Kami tidak menemukan akun dengan email tersebut.",

];
