<?php

return [
	'action'	=> [
		'checkout'		=> 'Pesan',
		'checkout_vt'	=> 'Lanjut ke Pembayaran',
		'choose'		=> 'Pilih ini',
		'subscribe'		=> 'Berlangganan',
	],
	'has_subscription'	=> 'Saat ini Anda sedang berlangganan layanan Gakken hingga :expire',
	'no_description'	=> 'Tidak ada deskripsi',
	'order'		=> [
		'confirm'	=> [
			'amount'			=> 'Jumlah Transfer',
			'back_orders'		=> 'Kembali ke Riwayat Pemesanan',
			'bank'				=> 'Bank',
			'help'				=> 'Silahkan isi formulir berikut untuk mengkonfirmasi pembayaran Anda',
			'from_account'		=> 'Dari Rekening Anda',
			'from_account_no'	=> 'No. Rekening',
			'from_account_name'	=> 'Nama pada Rekening',
			'order_no'			=> 'Untuk No. Pesanan',
			'order_no_help'		=> 'Anda dapat melihat pesanan Anda di',
			'ref_no'			=> 'No. Referensi (jika ada)',
			'save'				=> 'Simpan',
			'title'				=> 'Konfirmasi Pembayaran',
			'to_account'		=> 'Ke Rekening Gakken',
			'to_account_choice'	=> 'Rekening Tujuan',
			'transfer_date'		=> 'Tanggal Transaksi'
		],
		'status'	=> [
			'approved'	=> 'Disetujui',
			'pending'	=> 'Pending',
			'settled'	=> 'Lunas',
			'success'	=> 'Pembayaran Sukses',
		],
	],
	'self'		=> [
		'empty'					=> 'Anda belum membeli paket berlangganan apapun.',
		'no_billing_details'	=> 'Anda harus mengisi rincian penagihan sebelum berlangganan.',
		'orders'				=> [
			'created_at'		=> 'Dipesan pada',
			'empty'				=> 'Anda belum memesan paket berlangganan apapun',
			'subscription'		=> 'Paket Berlangganan',
			'order_no'			=> 'No. Pesanan',
			'order_received'	=> 'Pesanan Anda untuk :subscription telah diterima. Silahkan melakukan pembayaran dalam waktu 24 jam.',
			'quantity'			=> 'Jumlah',
			'status'			=> 'Status',
			'status_pending'	=> 'Pending',
			'status_success'	=> 'Pembayaran Sukses',
			'title'				=> 'Riwayat Pemesanan',
			'total'				=> 'Total',
		],
		'title'					=> 'Berlangganan',
	],
	'subscribe'	=> [
		'options'				=> [
			'monthly'	=> 'Bulanan',
			'yearly'	=> 'Tahunan',
		],
		'payment_method'		=> [
			'credit_card'		=> 'Kartu Kredit/Debit',
			'banktransfer'		=> 'Transfer Bank',
			'title'				=> 'Metode Pembayaran',
			'vtweb'				=> 'Kartu Kredit, Kartu Debit',
			'vtwebinstallment'	=> 'Cicilan Kartu Kredit',
		],
		'periods' 				=> [
			'm'	=> 'bulan',
			'y'	=> 'tahun'
		],
		'title_new'				=> 'Pilih Paket Berlangganan',
		'title_change'			=> 'Upgrade Paket Berlangganan',
		'veritrans_disclaimer'	=> 'Anda akan diarahkan ke halaman pembayaran Midtrans'
	]
];