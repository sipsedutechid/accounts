@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('account.settings.billing.title')]) }} @endsection

@section('user-content')

	<h2>{{ trans('account.action.settings') }}</h2>
	<br />

	@include('account.settings_tabs')
	@include('layout.alert')

	{!! Form::model($details, ['url' => '/my/settings/billing', 'class' => 'form-horizontal']) !!}

		@if (Request::input('ref')) {!! Form::hidden('redirect', Request::input('ref')) !!} @endif

		<div class="form-group">
			<label class="control-label col-lg-4">{{ trans('account.settings.billing.address_1') }}</label>
			<div class="col-lg-8"> 
				{!! Form::text('address_1', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-4">{{ trans('account.settings.billing.address_2') }}</label>
			<div class="col-lg-8"> 
				{!! Form::text('address_2', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-4">{{ trans('account.settings.billing.city') }}</label>
			<div class="col-lg-4"> 
				{!! Form::text('city', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-4">{{ trans('account.settings.billing.postal_code') }}</label>
			<div class="col-lg-4"> 
				{!! Form::text('postal_code', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-4">{{ trans('account.settings.billing.phone') }}</label>
			<div class="col-lg-4"> 
				{!! Form::text('phone', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-4">{{ trans('account.settings.billing.country_code') }}</label>
			<div class="col-lg-4"> 
				{!! Form::select('country_code', ['IDN' => 'Indonesia'], null, ['class' => 'form-control', 'placeholder' => trans('app.dropdown_placeholder')]) !!}
			</div>
		</div>

		<div style="margin-top:30px;" class="text-right">
			<button type="submit" class="btn btn-primary">{{ trans('account.settings.billing.submit') }}</button>
		</div>

	{!! Form::close() !!}

@endsection