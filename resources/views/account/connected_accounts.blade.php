@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('account.connected_accounts')]) }} @endsection

@section('user-content') 

	<h2>{{ trans('account.action.settings') }}</h2>
	<br />

	@include('account.settings_tabs')
	@include('layout.alert')

	@if (count($accounts))
		<table class="table table-hover">
			<thead>
				<tr><th colspan="3">Daftar Akun</th></tr>
			</thead>
			<tbody>
				@foreach($accounts as $account)
					<tr>
						<td>{{ ucfirst($account->provider) }}</td>
						<td>{{ $account->email }}</td>
					</tr>
				@endforeach
			</tbody>
		</ul>
	@else
		<p>{{ trans('account.no_connected_accounts') }}</p>
	@endif

@endsection