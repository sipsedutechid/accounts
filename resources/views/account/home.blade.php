@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('account.title') ]) }} @endsection

@section('user-content')

	@include('layout.alert')

	<div class="row">
		<div class="col-lg-3 col-sm-4 col-xs-8 col-sm-offset-0 col-xs-offset-2">
			<a href="/my" class="profile">
				<img src="{{ auth()->user()->avatar_url ?: asset('/assets/img/placeholder_avatar.png') }}" />
			</a>
		</div>

		<div class="col-lg-9 col-sm-8">
			<div class="h1">{{ auth()->user()->name }}</div>
			<p class="text-muted small">
				{{ trans('account.created_at', ['date' => auth()->user()->created_at]) }}<br />
				{{ auth()->user()->email }}<br />
				@if (auth()->user()->verified_at)
					<span class="label label-success">{{ trans('account.verified') }}</span>
				@else
					<a href="/account/verify">{{ trans('account.action.verify') }}</a>
				@endif
			</p>
			@if (isset(auth()->user()->userDetail->profession))
				<p class="text-muted small">
					{{ trans('account.profession.' . auth()->user()->userDetail->profession) }} / 
					{{ trans('account.profession.' . auth()->user()->userDetail->profession . '_id') }}: {{ auth()->user()->userDetail->id_no }}
				</p>
			@endif

			@if (auth()->user()->currentSubscription())
				<p>
					<a href="/my/subscriptions">
						{{ trans('account.has_subscription') }}
					</a>
				</p>
			@endif

		</div>
	</div>

	<h3 class="page-header">{{ trans('account.billing_details') }}</h3>
	@if (!$user->userDetail)
		<p style="margin-top:15px;">{{ trans('account.no_billing_details') }} <a href="/my/settings/billing">{{ trans('account.action.complete_billing') }}</a></p>
	@else
		<p style="margin-top:15px;">
			{{ $user->userDetail->address_1 }}<br /> 
			@if ($user->userDetail->address_2)
				{{ $user->userDetail->address_2 }}<br />
			@endif
			{{ $user->userDetail->city }} - {{ $user->userDetail->postal_code }}<br />

			<a href="/my/settings/billing" class="small">{{ trans('account.action.edit_billing') }}</a>
		</p>
	@endif

@endsection