@extends('layout.user')

@section('title')
	{{ trans('app.page_title', ['title' => trans('account.settings.password.title_' . (auth()->user()->is_provider ? 'new' : 'edit'))]) }}
@endsection

@section('stylesheets')
	<link rel="stylesheet" type="text/css" href="{{ asset('/bower_components/Strength.js/src/strength.css') }}" />
	<style type="text/css">
		[name=new] { border-bottom-left-radius: 0; border-bottom-right-radius: 0; }
		.button_strength { display: none; }
		.strength_meter { float: left; width:100%; }
	</style>
@stop

@section('user-content')
	
	<h2>{{ trans('account.action.settings') }}</h2>
	<br />
	
	@include('account.settings_tabs')
	@include('layout.alert')

	{!! Form::open(['url' => '/my/settings/password', 'class' => 'form-horizontal']) !!}
		{!! Form::hidden('status', auth()->user()->is_provider ? 'new' : 'change') !!}

		@if (!auth()->user()->is_provider)
			<div class="form-group">
				<label class="control-label col-sm-4">{{ trans('account.settings.password.old') }}</label>
				<div class="col-sm-8">
					{!! Form::password('old', ['class' => 'form-control', 'autofocus' => 'autofocus']) !!}
				</div>
			</div>
		@endif

		<div class="form-group">
			<label class="control-label col-sm-4">{{ trans('account.settings.password.new') }}</label>
			<div class="col-sm-8">
				{!! Form::password('new', ['class' => 'form-control', 'id' => 'newPassword']) !!}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">{{ trans('account.settings.password.new_confirmation') }}</label>
			<div class="col-sm-8">
				{!! Form::password('new_confirmation', ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="text-right" style="margin-top:30px;">
			<button type="submit" class="btn btn-primary">{{ trans('account.settings.password.save') }}</button>
		</div>
	{!! Form::close() !!}

@stop

@section('scripts')
	<script type="text/javascript" src="{{ asset('/bower_components/Strength.js/src/strength.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$("#newPassword").strength();
		});
	</script>
@stop