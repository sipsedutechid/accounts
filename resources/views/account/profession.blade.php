@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('account.details.title')]) }} @endsection

@section('user-content')

	<h1>{{ trans('account.action.settings') }}</h1>

	@include('account.settings_tabs')
	@include('layout.alert')

	{!! Form::model($details, ['url' => '/my/settings/profession', 'class' => 'form-horizontal']) !!}

		@if (Request::input('ref')) {!! Form::hidden('redirect', Request::input('ref')) !!} @endif

		@if (!$details->profession)
			<div class="form-group">
				<label class="control-label col-lg-4">{{ trans('account.details.field.profession') }}</label>
				<div class="col-lg-8">
					<div class="radio">
						<label>{!! Form::radio('profession', 'doctor', $details->profession == 'doctor') !!} {{ trans('account.details.field.profession_doctor') }}</label>&ensp;
						<label>{!! Form::radio('profession', 'dentist', $details->profession == 'dentist') !!} {{ trans('account.details.field.profession_dentist') }}</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-4">{{ trans('account.details.field.id_no') }}</label>
				<div class="col-lg-3"> 
					{!! Form::text('id_no', null, ['disabled' => 'disabled', 'class' => 'form-control']) !!}
				</div>
			</div>
		@else
			<p style="margin-top: 15px;margin-bottom:30px;">
				{{ trans('account.details.field.profession_' . $details->profession) }} - {{ trans('account.profession.' . $details->profession . '_id') }}: {{ $details->id_no }}&ensp;
				<a href="#" data-toggle="modal" data-target="#changeProfessionModal">Change</a>
			</p>
		@endif

	{!! Form::close() !!}
	<p style="margin-top:30px;"><a href="/my">{{ trans('account.action.home') }}</a></p>

@endsection