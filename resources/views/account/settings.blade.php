@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('account.action.settings')]) }} @endsection

@section('user-content')

	<h2>{{ trans('account.action.settings') }}</h2>
	<br />

	@include('account.settings_tabs')
	@include('layout.alert')

	{!! Form::open(['url' => '/my/settings', 'class' => 'form-horizontal', 'files' => true]) !!}
		<div class="form-group">
			<label class="control-label col-sm-3">{{ trans('account.settings.general.email') }}</label>
			<div class="col-sm-9">
				<p class="form-control-static">
					{{ auth()->user()->email }}&ensp;
					@if (auth()->user()->verified_at)
						<span class="label label-success">{{ trans('account.verified') }}</span>
					@else
						<a href="/account/verify">{{ trans('account.action.verify') }}</a>
					@endif
				</p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3">{{ trans('account.settings.general.name') }}</label>
			<div class="col-sm-9">
				{!! Form::text('name', auth()->user()->name, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3">{{ trans('account.settings.general.avatar') }}</label>
			<div class="col-sm-9">
				<div class="user-profile" style="display:inline-block; background: url('{{ auth()->user()->avatar_url ?: asset('/assets/img/placeholder_avatar.png') }}') no-repeat; background-size: cover; background-position: 50% 50%;width:150px;height:150px;border-radius:50%;"></div>
				{!! Form::file('avatar', ['class' => 'form-control']) !!}
				<div class="help-block small text-muted">{{ trans('account.settings.general.avatar_help') }}</div>
			</div>
		</div>

		@if (!auth()->user()->profession)
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('account.settings.general.profession') }}</label>
				<div class="col-sm-9">
					<div class="radio">
						<label>{!! Form::radio('profession', 'doctor', auth()->user()->profession == 'doctor') !!} {{ trans('account.settings.general.profession_doctor') }}</label>&ensp;
						<label>{!! Form::radio('profession', 'dentist', auth()->user()->profession == 'dentist') !!} {{ trans('account.settings.general.profession_dentist') }}</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('account.settings.general.id_no') }}</label>
				<div class="col-sm-5"> 
					{!! Form::text('id_no', null, ['disabled' => 'disabled', 'class' => 'form-control']) !!}
				</div>
			</div>
		@else
			<div class="form-group">
				<label class="control-label col-sm-3">{{ trans('account.settings.general.profession') }}</label>
				<div class="col-sm-9">
					<p class="form-control-static">
						{{ trans('account.settings.general.profession_' . auth()->user()->profession) }} - {{ trans('account.profession.' . auth()->user()->profession . '_id') }}: {{ auth()->user()->id_no }}&ensp;
						<a href="#" data-toggle="modal" data-target="#changeProfessionModal">{{ trans('app.action.change') }}</a>
					</p>
				</div>
			</div>
		@endif

		<div class="text-right" style="margin-top:30px;">
			<button type="submit" class="btn btn-primary">{{ trans('account.settings.general.save') }}</button>
		</div>

	{!! Form::close() !!}

@endsection

@section('modals')
	
	<div class="modal fade" id="changeProfessionModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h3>{{ trans('account.settings.general.edit_profession_title') }}</h3>
					<p>{{ trans('account.settings.general.edit_profession_text', ['hotline' => '0800-1-401652', 'email' => 'support@gakken-idn.co.id']) }}</p>

					<div class="text-right">
						<a href="#" data-dismiss="modal" class="btn">{{ trans('app.action.done') }}</a>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')

	<script type="text/javascript">
		$(document).ready(function () {
			var _profession = $("input[name=profession]"),
				_idNo = $("input[name=id_no]");

			_resetIdNo();
			_profession.on('change', function() { _resetIdNo(); });

			function _resetIdNo() {
				var _selectedProfession = $("input[name=profession]:checked");
				if (_selectedProfession.length)
					return _idNo.removeAttr("disabled").focus();

				_idNo.attr("disabled", "disabled");
			}
		});
	</script>

@endsection