
<ul class="nav nav-tabs" style="padding:0;margin-bottom:30px;">
	<li class="small {{ $tab == 'general' ? 'active' : '' }}" style="padding:0;"><a href="/my/settings" style="margin:0;">{{ trans('account.settings.tabs.general') }}</a></li>
	<li class="small {{ $tab == 'password' ? 'active' : '' }}" style="padding:0;"><a href="/my/settings/password" style="margin:0;">{{ trans('account.settings.tabs.password') }}</a></li>
	<li class="small {{ $tab == 'billing' ? 'active' : '' }}" style="padding:0;"><a href="/my/settings/billing" style="margin:0;">{{ trans('account.settings.tabs.billing') }}</a></li>
	<li class="small {{ $tab == 'accounts' ? 'active' : '' }}" style="padding:0;"><a href="/my/settings/accounts" style="margin:0;">{{ trans('account.settings.tabs.accounts') }}</a></li>
</ul>