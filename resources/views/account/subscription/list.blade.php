@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('subscription.self.title')]) }} @endsection

@section('user-content')

	@include('layout.alert')

	<h2>{{ trans('subscription.self.title') }}</h2>

	@if (!isset(auth()->user()->userDetail))
		<p>
			{{ trans('subscription.self.no_billing_details') }}
			<a href="/my/settings/billing?ref={{ urlencode(url()->current()) }}">{{ trans('account.action.complete_billing') }}</a>
		</p>
	@else
		@if ($current)
			<p>{{ trans('subscription.has_subscription', ['expire' => date('j M Y', strtotime($current->expired_at))]) }}</p>
		@else
			<p>
				{{ trans('subscription.self.empty') }}
				<a href="/subscribe">{{ trans('subscription.action.subscribe') }}</a>
			</p>
		@endif
		
		<h4 class="page-header" style="margin-top:30px;">{{ trans('subscription.self.orders.title') }}</h4>
		@if (count($orders))
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th style="width: 150px;">{{ trans('subscription.self.orders.order_no') }}</th>
						<th style="width: 120px;">{{ trans('subscription.self.orders.created_at') }}</th>
						<th>{{ trans('subscription.self.orders.status') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
						<tr>
							<td>{{ $order->order_no }}</td>
							<td>{{ $order->created_at }}</td>
							<td>
								{{ trans('subscription.order.status.' . $order->status) }}
								({{ trans('subscription.subscribe.payment_method.' . $order->method) }})
								@if (in_array($order->status, ['success', 'settled']))
									<a href="/my/invoices/{{ $order->order_no }}" target="_blank">View Invoice</a>
								@elseif ($order->method == 'banktransfer')
									<a href="/order/confirm?order_no={{ $order->order_no }}">Confirm Payment</a>
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		@else
			<p>{{ trans('subscription.self.orders.empty') }}</p>
		@endif

	@endif

	<p style="margin-top:30px;">
		@if (Request::input('ref'))
			<a href="{{ Request::input('ref') }}">{{ trans('app.action.back') }}</a>
		@else
			<a href="/my">{{ trans('account.action.home') }}</a>
		@endif
	</p>

@endsection