@extends('layout.guest')

@section('title') {{ trans('app.page_title', ['title' => trans('auth.login.page_title')]) }} @endsection

@section('guest-content')

<div class="row">
	<div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3" style="padding-top: 45px;padding-bottom: 45px;">
		<h3 class="text-center">{{ trans('auth.login.title') }}</h3>

		<p class="text-center">
			{{ trans('auth.register.login_help') }}
			<a href="/register">{{ trans('auth.action.register') }}</a>
		</p>

		<div class="login-container">
			<div id="message" class="alert alert-danger">
				@if (count($errors)) {{ $errors->first() }} @endif
			</div>

			{!! Form::open(['url' => Request::fullUrl(), 'id' => 'loginForm', 'style' => 'margin-bottom:15px;']) !!}

				@if (Request::input('redirect'))
					{!! Form::hidden('redirect', Request::input('redirect')) !!}
				@endif
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><b class="fa fa-fw fa-envelope"></b></span>
						{!! Form::email('email', null, ['class' => 'form-control input-lg', 'placeholder' => 'Alamat Email', 'autofocus' => 'autofocus']) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><b class="fa fa-fw fa-lock"></b></span>
						{!! Form::password('password', ['class' => 'form-control input-lg', 'placeholder' => 'Kata Sandi']) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label>
							{!! Form::checkbox('remember') !!}
							{{ trans('auth.login.remember_me') }}
							<span class="small">{{ trans('auth.login.remember_me_help') }}</span>
						</label>
					</div>
				</div>

				<p style="margin-bottom:15px;" class="text-center"><a href="/recover">{{ trans('auth.action.forgot') }}</a></p>
				<button type="submit" class="btn btn-block btn-primary">{{ trans('auth.login.submit') }}</button>

			{!! Form::close() !!}

			<hr />

			<div>
				<a href="/oauth/google?ref=login" class="btn btn-block btn-google">
					<b class="fa fa-fw fa-google-plus"></b>
					{{ trans('auth.login.google') }}
				</a>
				<a href="/oauth/facebook?ref=login" class="btn btn-block btn-facebook">
					<b class="fa fa-fw fa-facebook"></b>
					{{ trans('auth.login.facebook') }}
				</a>
			</div>
			<p class="small text-center" style="margin-top: 30px;">
				<a href="/terms-conditions">Terms and Conditions</a> |
				<a href="/privacy-policy">Privacy Policy</a>
			</p>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	
    <script src="{{ asset('/assets/js/home.js') }}" crossorigin="anonymous"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			var _form = $("#loginForm"),
				_message = $("#message");

			if (!_message.text().trim().length)
				_message.html('').hide();

			_form.submit(function (e) {
				e.preventDefault();
				_message.html('').hide();
				_form.find('[type=submit]').attr('disabled', 'disabled');
				$.post(_form.attr('action'), _form.serialize(), function (r) {
					if (r.authenticated == false) {
						_message.text(r.error).show();
						_form.find('[type=submit]').removeAttr('disabled');
						return;
					}
					window.location = r.intended == undefined ? '/my' : r.intended;
				}, 'json').error(function () {
					_form.find('[type=submit]').removeAttr('disabled');
				});
			});

		});
	</script>

@endsection