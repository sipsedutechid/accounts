@extends('layout.guest')

@section('title') {{ trans('app.page_title', ['title' => trans('auth.forgot.title')]) }} @endsection

@section('stylesheets')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/auth.css') }}" media="screen" />
@endsection

@section('guest-content')

	<div class="recover-wrapper">
    	<div class="recover-form col-xs-12">
			<h1>{{ trans('auth.forgot.title') }}</h1>
			<p>{{ trans('auth.forgot.help') }}</p>

			<div class="row">
				<div class="col-md-5 col-sm-6">
					{!! Form::open(['url' => '/recover']) !!}

						<div class="form-group">
							<label>{{ trans('auth.forgot.email') }}</label>
							{!! Form::email('email', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) !!}
						</div>

						<button type="submit" class="btn btn-default">{{ trans('auth.forgot.submit') }}</button>

					{!! Form::close() !!}
				</div>
			</div>
			
			<h4 style="margin-top:30px;"><a href="/login">{{ trans('auth.register.to_login') }}</a></h4>
		</div>
	</div>

@endsection

@section('scripts')

    <script src="{{ asset('/assets/js/home.js') }}" crossorigin="anonymous"></script>

@endsection