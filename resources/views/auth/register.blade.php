@extends('layout.guest')

@section('title') {{ trans('app.page_title', ['title' => trans('auth.register.title')]) }} @endsection

@section('guest-content')

	<div style="background:url('{{ asset('assets/img/register_comp.jpg') }}') center center no-repeat;background-size:cover;margin:-30px -30px -45px -30px;">
    <div class="row">
    	<div class="col-md-5">
            <div style="background: rgba(255,255,255,0.6);padding:30px 30px 45px 30px;">
			<h3>{{ trans('auth.register.title') }}</h3>

			@if (count($errors))
				<p>{{ $errors->first() }}</p>
			@endif

			{!! Form::open(['url' => '/register']) !!}

				<div class="form-group">
					<label>{{ trans('auth.register.field.name') }}</label>
					{!! Form::text('name', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) !!}
				</div>
				<div class="form-group">
					<label>{{ trans('auth.register.field.email') }}</label>
					{!! Form::email('email', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					<label>{{ trans('auth.register.field.password') }}</label>
					{!! Form::password('password', ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					<label>{{ trans('auth.register.field.password_confirm') }}</label>
					{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label>
    						{!! Form::checkbox('agreement', true) !!}
    						Saya telah membaca <a href="{{ env('ROOT_APP_URL', '') }}/syarat-dan-ketentuan">Syarat dan Ketentuan</a>
						</label>
					</div>
				</div>

				<div class="form-group"><div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_SITE') }}"></div></div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary">{{ trans('auth.register.submit') }}</button>
				</div>
				<p><a href="/login">{{ trans('auth.register.to_login') }}</a></p>

			{!! Form::close() !!}
            </div>
		</div>
	</div>
	</div>

@endsection

@section('scripts')

	<script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{ asset('/assets/js/home.js') }}" crossorigin="anonymous"></script>

@endsection