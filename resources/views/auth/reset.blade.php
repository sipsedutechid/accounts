@extends('layout.guest')

@section('title') {{ trans('app.page_title', ['title' => trans('auth.forgot.title')]) }} @endsection

@section('stylesheets')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/auth.css') }}" media="screen" />
@endsection	

@section('guest-content')

	<div class="reset-wrapper">
		<div class="reset-form col-xs-12">
			<h1>{{ trans('auth.forgot.title') }}</h1>

			<div class="row">
				<div class="col-md-5 col-sm-6">
					{!! Form::open(['url' => '/reset']) !!}
						{!! Form::hidden('token', $token) !!}
						<p>
							<label>Email Address</label><br />
							{!! Form::email('email', null, ['autofocus' => 'autofocus', 'class' => 'form-control']) !!}
						</p>
						<p>
							<label>Set a New Password</label><br />
							{!! Form::password('password', ['class' => 'form-control']) !!}
						</p>
						<p>
							<label>Confirm Password</label><br />
							{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
						</p>
						<button type="submit" class="btn btn-default">Reset Password</button>

					{!! Form::close() !!}
				</div>
			</div>
			
			<h4 style="margin-top:30px;"><a href="/login">{{ trans('auth.register.to_login') }}</a></h4>
		</div>
	</div>

@endsection