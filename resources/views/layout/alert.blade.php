@if (count($errors))
	<div class="alert alert-danger">{{ $errors->first() }}</div>
@elseif (Request::session()->get('msg-info'))
	<div class="alert alert-info">
		<p>{{ Request::session()->get('msg-info') }}</p>
	</div>
@elseif (Request::session()->get('msg-success'))
	<div class="alert alert-success">
		<p>{{ Request::session()->get('msg-success') }}</p>
	</div>
@elseif (Request::session()->get('msg-warning'))
	<div class="alert alert-warning">
		<p>{{ Request::session()->get('msg-warning') }}</p>
	</div>
@elseif (Request::session()->get('msg-danger'))
	<div class="alert alert-danger">
		<p>{{ Request::session()->get('msg-danger') }}</p>
	</div>
@endif