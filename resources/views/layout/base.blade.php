<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@yield('title')</title>

		<link rel="manifest" href="{{ asset('assets/gakken-accounts.webmanifest') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.css') }}" />
		@yield('stylesheets')

		<script type="text/javascript" src="{{ asset('libs/jquery/jquery.js') }}"></script>
		<script type="text/javascript">
			$.ajaxSetup({
        		headers: {
            		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        		}
			});
		</script>
	</head>
	<body>

		@yield('content')

		@yield('modals')
		
		<script src="{{ asset('/assets/js/bootstrap.min.js') }}" crossorigin="anonymous"></script>
	    <script src="{{ asset('/assets/js/bootstrap-sprockets.js') }}" crossorigin="anonymous"></script>
		@yield('scripts')
	</body>

</html>