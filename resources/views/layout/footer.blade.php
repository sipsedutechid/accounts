<footer class="footer">
  	<section id="footer">
    	<div class="container">
      		<div class="row hidden-xs">

        		<div class="col-sm-4 hidden-sm hidden-xs">
          			<img src="{{ asset('/assets/img/logo.png') }}" style="width:100%;max-width:200px;" />
          			<p class="small" style="margin-top:15px;">
			            PT. Gakken Health and Education Indonesia<br />
			            Kompleks Ruko Ratulangi<br />
			            Jl. Dr. Sam Ratulangi No. 7/C9<br />
			            Makassar, Indonesia
          			</p>
          			<p class="small">contact@gakken-idn.co.id</p>
    			</div>

        		<div class="col-md-2 col-sm-3 widget">
        			<h3>Kesehatan</h3>
        			<div class="menu-footer-links-health-container">
        				<ul id="menu-footer-links-health" class="menu">
        					<li id="menu-item-119" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-119">
        						<a href="https://eduhealth.co.id/artikel/">Artikel</a>
        					</li>
        					<li id="menu-item-120" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-120">
        						<a href="https://eduhealth.co.id/jurnal/">Jurnal</a>
        					</li>
        					<li id="menu-item-122" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-122">
        						<a href="https://eduhealth.co.id/unduh/">Unduhan</a>
        					</li>
        					<li id="menu-item-121" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-121">
        						<a href="https://eduhealth.co.id/obat/">Indeks Obat</a>
        					</li>
      					</ul>
      				</div>
      			</div>
      			<div class="col-md-2 col-sm-3 widget">
      				<h3>Kerjasama</h3>
      				<div class="menu-footer-links-external-container">
      					<ul id="menu-footer-links-external" class="menu">
      						<li id="menu-item-128" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-128">
      							<a href="https://gakken-idn.id" onclick="__gaTracker('send', 'event', 'outbound-widget', 'https://gakken-idn.id', 'Gakken P2KB');">Gakken P2KB</a>
      						</li>
        					<li id="menu-item-129" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-129">
        						<a href="http://gakken-idn.co.id">PT. Gakken Health and Education Indonesia</a>
        					</li>
      					</ul>
      				</div>
      			</div>
      			<div class="col-md-4 col-sm-3 widget">
      				<h3>Newsletter</h3>
      				<div class="textwidget">
      					<!-- Begin MailChimp Signup Form -->
    					<style type="text/css">
					        #mc_embed_signup{clear:left; font:14px Helvetica,Arial,sans-serif;}
					        #mc_embed_signup form{padding: 0;}
        				</style>
        				<div id="mc_embed_signup">
        					<form action="//gakken-idn.us13.list-manage.com/subscribe/post?u=8fd0913ea46d91bec993867fd&amp;id=30fba8f95d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        						<div id="mc_embed_signup_scroll">
        							<div class="mc-field-group">
        								<label for="mce-EMAIL">Email <span class="asterisk">*</span></label>
        								<input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
        							</div>
        							<div id="mce-responses" class="clear">
        								<div class="response" id="mce-error-response" style="display:none"></div>
        								<div class="response" id="mce-success-response" style="display:none"></div>
        							</div>
        							<div style="position: absolute; left: -5000px;" aria-hidden="true">
        								<input type="text" name="b_8fd0913ea46d91bec993867fd_30fba8f95d" tabindex="-1" value="">
        							</div>
        							<div class="clear">
        								<button type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary" style="background-color: #EC1F27;color: #fff;border-color: #D20811;margin-top:5px;">Subscribe</button>
        							</div>
        						</div>
        					</form>
        				</div>
        				<!--End mc_embed_signup-->
        			</div>
            	</div>
            </div>

            <div class="small text-center" style="margin-top:45px;">
            	Copyright 2016 &copy; PT. Gakken Health and Education Indonesia. All Rights Reserved.
	        </div>
    	</div>
    </section>
</footer>