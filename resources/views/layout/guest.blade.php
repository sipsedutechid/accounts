@extends('layout.base')

@section('content')

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapsible" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="logo">
                    <a class="navbar-brand" href="{{ env('ROOT_APP_URL', '') }}">
                        <img src="{{ asset('assets/img/logo-gakken-indonesia.png') }}" />
                    </a>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="navbar-collapsible">
                <ul class="nav navbar-nav">
                    <li><a href="{{ env('ROOT_APP_URL') }}">Beranda</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Artikel Kesehatan <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ env('ROOT_APP_URL') }}/kategori/berita/">Berita</a></li>
                            <li><a href="{{ env('ROOT_APP_URL') }}/kategori/darah-hematologi/">Darah / Hematologi</a></li>
                            <li><a href="{{ env('ROOT_APP_URL') }}/kategori/gigi/">Gigi</a></li>
                            <li><a href="{{ env('ROOT_APP_URL') }}/kategori/kehamilan-obstetri/">Kehamilan / Obstetri</a></li>
                            <li><a href="{{ env('ROOT_APP_URL') }}/kategori/kesehatan-anak/">Kesehatan Anak</a></li>
                            <li><a href="{{ env('ROOT_APP_URL') }}/kategori/penyakit/">Penyakit</a></li>
                            <li><a href="{{ env('ROOT_APP_URL') }}/kategori/tips-kesehatan/">Tips Kesehatan</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Referensi <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ env('ROOT_APP_URL') }}/jurnal">Jurnal</a></li>
                            <li><a href="{{ env('ROOT_APP_URL') }}/obat">Indeks Obat</a></li>
                            <li><a href="{{ env('ROOT_APP_URL') }}/unduh">Media dan Data</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ env('ROOT_APP_URL') }}/topik">Topik</a></li>
                    <li><a href="{{ env('ROOT_APP_URL') }}/genius">GENIUS</a></li>
                    <li><a href="{{ env('ROOT_APP_URL') }}/berlangganan/">Berlangganan</a></li>
                </ul>
                <div class="navbar-right">
                    <a href="/login" class="btn btn-primary navbar-btn highlight-btn">Sign in</a>
                </div>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="main-container">

            @yield('guest-content')

        </div>
    </div>

@endsection