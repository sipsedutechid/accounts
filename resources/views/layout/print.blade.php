<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<title>@yield('title')</title>

		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/print.css') }}" media="print" />
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/styles.css') }}" />
		@yield('stylesheets')

		<script type="text/javascript" src="{{ asset('libs/jquery/jquery.js') }}"></script>
	</head>
	<body>

		<div class="container">
			@yield('content')
		</div>

		<script src="{{ asset('/assets/js/bootstrap.min.js') }}" crossorigin="anonymous"></script>
	    <script src="{{ asset('/assets/js/bootstrap-sprockets.js') }}" crossorigin="anonymous"></script>
	    <script type="text/javascript">
	    	$(document).ready(function () {
	    		// window.print();
	    	});
	    </script>
		@yield('scripts')
	</body>
</html>