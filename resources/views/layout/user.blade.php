@extends('layout.base')

@section('content')

	<nav class="navbar navbar-default navbar-fixed-top">
  		<div class="container">
            <div class="navbar-header">
      			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapsible" aria-expanded="false">
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
      			</button>
      			<div class="logo">
      				<a class="navbar-brand" href="{{ env('ROOT_APP_URL', '') }}">
      					<img src="{{ asset('assets/img/logo-gakken-indonesia.png') }}" />
      				</a>
      			</div>
    		</div>

            <div class="collapse navbar-collapse" id="navbar-collapsible">
		      	<ul class="nav navbar-nav">
		      		<li><a href="{{ env('ROOT_APP_URL') }}">Beranda</a></li>
		      		<li class="dropdown">
		      			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Artikel Kesehatan <span class="caret"></span></a>
		      			<ul class="dropdown-menu">
		      				<li><a href="{{ env('ROOT_APP_URL') }}/kategori/berita/">Berita</a></li>
							<li><a href="{{ env('ROOT_APP_URL') }}/kategori/darah-hematologi/">Darah / Hematologi</a></li>
							<li><a href="{{ env('ROOT_APP_URL') }}/kategori/gigi/">Gigi</a></li>
							<li><a href="{{ env('ROOT_APP_URL') }}/kategori/kehamilan-obstetri/">Kehamilan / Obstetri</a></li>
							<li><a href="{{ env('ROOT_APP_URL') }}/kategori/kesehatan-anak/">Kesehatan Anak</a></li>
							<li><a href="{{ env('ROOT_APP_URL') }}/kategori/penyakit/">Penyakit</a></li>
							<li><a href="{{ env('ROOT_APP_URL') }}/kategori/tips-kesehatan/">Tips Kesehatan</a></li>
		      			</ul>
		      		</li>
		      		<li class="dropdown">
		      			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Referensi <span class="caret"></span></a>
		      			<ul class="dropdown-menu">
		      				<li><a href="{{ env('ROOT_APP_URL') }}/jurnal">Jurnal</a></li>
				      		<li><a href="{{ env('ROOT_APP_URL') }}/obat">Indeks Obat</a></li>
				      		<li><a href="{{ env('ROOT_APP_URL') }}/unduh">Media dan Data</a></li>
		      			</ul>
		      		</li>
		      		<li><a href="{{ env('ROOT_APP_URL') }}/topik">Topik</a></li>
		      		<li><a href="{{ env('ROOT_APP_URL') }}/genius">GENIUS</a></li>
		      		<li><a href="{{ env('ROOT_APP_URL') }}/berlangganan/">Berlangganan</a></li>
      			</ul>
      			@if (auth()->check())
	      			<ul class="nav navbar-nav navbar-right">
	      			  	<li class="dropdown user-profile-wrap">
	      			    	<a href="#" class="dropdown-toggle user-profile-wrap-btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		      			    	<span style="float: left; padding: 7px 10px;">{{ auth()->user()->name }}</span>
	      			    		<div class="user-profile" style="background-image: url('{{ auth()->user()->avatar_url ?: asset('/assets/img/placeholder_avatar.png') }}');"></div>
	      			    		<div class="clearfix"></div>
	      			    	</a>
	      			    	<ul class="dropdown-menu user-tab">
	      				      	<li><a href="/my">{{ trans('account.title') }}</a></li>
	      				      	<li><a href="/my/subscriptions">{{ trans('account.action.subscriptions') }}</a></li>
	      				      	<li role="separator" class="divider"></li>
	      				      	<li><a href="/logout">{{ trans('auth.action.logout')}}</a></li>
	      			    	</ul>
	      			  	</li>
	      			</ul>
      			@else
	      			<div class="navbar-right">
	                    <a href="/login" class="btn btn-primary navbar-btn highlight-btn">Sign in</a>
	      			</div>
      			@endif
    		</div>
  		</div>
	</nav>

    <div class="container">

      	<div class="main-container">

        	<div class="row" style="padding-top: 30px;">
        		<div class="col-md-3">

          			<div>
            			<h4>{{ trans('account.action.title') }}</h4>
        				<ul class="list-unstyled">
        					<li><a href="/my">Profilku</a></li>
        					<li><a href="/my/settings">{{ trans('account.action.settings') }}</a></li>
        					<li><a href="/my/settings/password">{{ trans('account.action.password') }}</a></li>
							<li><a href="/my/subscriptions">{{ trans('account.action.subscriptions') }}</a></li>
							<li><a href="/order/confirm">{{ trans('subscription.order.confirm.title') }}</a></li>
							<li><a href="/logout">{{ trans('auth.action.logout') }}</a></li>
    					</ul>
          			</div>

					@if(!auth()->user()->isUser() && !auth()->user()->isSubscriber())
	          			<div class="profile-container">
	            			<h4>{{ trans('admin.title_menu') }}</h4>
	            			<ul class="list-unstyled">
	        					<li><h5>{{ trans('admin.users.title') }}</h5></li>
	            				@can('general', 'accounts.users.view')
									<li><a href="/users/all">{{ trans('admin.users.list.title') }}</a></li>
								@endcan
								@can('general', 'accounts.roles.view')
									<li><a href="/roles/all">{{ trans('admin.roles.list.title') }}</a></li>
								@endcan
								@can('general', 'accounts.users.create')
									<li><a href="/users/register?ref=home">{{ trans('admin.users.register.title_new') }}</a></li>
								@endcan
								@can('general', 'accounts.user_groups.view')
									<li><a href="/users/groups/all">{{ trans('admin.users.groups.list.title') }}</a></li>
								@endcan
					        </ul>
	        				<ul class="list-unstyled">
	        					<li><h5>{{ trans('admin.orders.title') }}</h5></li>
	        					@can('general', 'accounts.orders.view')
									<li><a href="/orders/all">{{ trans('admin.orders.list.title') }}</a></li>
								@endcan
								@can('general', 'accounts.payment_confirmations.view')
								    <li><a href="/orders/confirmations/all">{{ trans('admin.orders.confirmations.title_list') }}</a></li>
								@endcan
	        				</ul>
	          			</div>
	          		@endif

        		</div>

        		<div class="col-md-9">
        			@yield('user-content')
        		</div>
        	</div>

      </div>
    </nav>
	<script type="text/javascript" src="{{ asset('assets/js/home.js') }}"></script>

@endsection
