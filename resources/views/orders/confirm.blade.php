@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('subscription.order.confirm.title')]) }} @endsection

@section('user-content')

	<h2>{{ trans('subscription.order.confirm.title') }}</h2>

	@if (count($errors))
		<div class="alert alert-danger">{{ $errors->first() }}</div>
	@else
		<p>{{ trans('subscription.order.confirm.help') }}</p>
	@endif
	<br />
	{!! Form::open(['url' => '/order/confirm', 'class' => 'form-horizontal']) !!}

		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('subscription.order.confirm.order_no') }}</label>
			<div class="col-sm-3">
				{!! Form::text('order_no', Request::input('order_no'), ['class' => 'form-control', 'autofocus' => 'autofocus']) !!}
			</div>
			<div class="col-sm-8 col-lg-offset-3 col-sm-offset-4">
				<div class="help-block small text-muted">
					{{ trans('subscription.order.confirm.order_no_help') }} <a href="/my/subscriptions?ref={!! urlencode(Request::fullUrl()) !!}">{{ trans('subscription.self.orders.title') }}</a>
				</div>
			</div>
		</div>

		<h4 class="page-header">{{ trans('subscription.order.confirm.from_account') }}</h4>

		<div class="form-group" style="margin-top:30px;">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('subscription.order.confirm.bank') }}</label>
			<div class="col-sm-8">
				{!! Form::text('bank', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('subscription.order.confirm.from_account_no') }}</label>
			<div class="col-sm-8">
				{!! Form::text('account_no', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('subscription.order.confirm.from_account_name') }}</label>
			<div class="col-sm-8">
				{!! Form::text('account_name', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<h4 class="page-header" style="margin-top:30px;">{{ trans('subscription.order.confirm.to_account') }}</h4>

		<div class="form-group" style="margin-top:30px;">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('subscription.order.confirm.to_account_choice') }}</label>
			<div class="col-sm-8">
				{!! Form::select('to_account', ['BNI46/1133113381' => 'BNI46 (1133113381)'], null, ['class' => 'form-control', 'placeholder' => trans('app.dropdown_placeholder')]) !!}
			</div>
		</div>

		<br />

		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('subscription.order.confirm.ref_no') }}</label>
			<div class="col-sm-8">
				{!! Form::text('ref_no', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('subscription.order.confirm.transfer_date') }}</label>
			<div class="col-sm-8">
				{!! Form::date('transfer_date', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('subscription.order.confirm.amount') }}</label>
			<div class="col-sm-8">
				<div class="input-group">
					<div class="input-group-addon">IDR</div>
					{!! Form::number('amount', null, ['min' => 0, 'class' => 'form-control text-right']) !!}
				</div>
			</div>
		</div>

		<br />

		<div class="form-group">
			<div class="col-sm-8 col-lg-offset-3 col-sm-offset-4">
				<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_SITE') }}"></div>
				<br />
				<button type="submit" class="btn btn-primary">{{ trans('subscription.order.confirm.save') }}</button>
			</div>
		</div>

		<p style="margin-top:30px;">
			@if (Request::input('order_no'))
				<a href="/my/subscriptions">{{ trans('subscription.order.confirm.back_orders') }}</a>
			@else
				<a href="/my">{{ trans('account.action.home') }}</a>
			@endif
		</p>

	{!! Form::close() !!}

@endsection

@section('scripts')
	<script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>

@endsection