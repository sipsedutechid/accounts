@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.orders.list.title')]) }} @endsection

@section('user-content')

	@include('layout.alert')

	<h2>{{ trans('admin.orders.list.title') }}</h2>

	<div style="margin-bottom:15px;">
		{!! Form::open(['url' => '/orders/all', 'class' => 'form-inline pull-right', 'method' => 'get']) !!}
			@if (count($orders))
				<label class="control-label">
					{{ trans('app.pagination', ['first' => $orders->firstItem(), 'last' => $orders->lastItem(), 'total' => $orders->total()]) }}
				</label>
			@endif
			<div class="input-group">
				{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => 'Search orders']) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default">Go</button>
				</span>
			</div>
		{!! Form::close() !!}
		<div class="clearfix"></div>
	</div>

	@if (count($orders))
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th style="width:150px;">Order No.</th>
					<th style="width:120px;">Ordered On</th>
					<th>Account</th>
					<th colspan="2">Total</th>
					<th style="width:130px;">Status</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($orders as $order)
					<tr>
						<td><a href="/orders/{!! urlencode($order->order_no) !!}">{{ $order->order_no }}</a></td>
						<td>{{ $order->created_at }}</td>
						<td>
							@can ('general', 'accounts.users.view')
								<a href="/users/{!! urlencode($order->user->email) !!}?ref=orders_list">{{ $order->user->name }}</a>
							@else
								{{ $order->user->name }}
							@endcan
						</td>
						<td style="width:40px;">IDR</td>
						<td style="width:90px;" class="text-right">{{ number_format($order->total, 0, '.', ',') }}</td>
						<td>{{ trans('subscription.order.status.' . $order->status) }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		{!! $orders->appends(['s' => Request::input('s')])->links() !!}
	@else
		<p>
			{{ trans('admin.orders.list.empty') }}
		</p>
	@endif

	<p>
		<a href="/my">{{ trans('account.action.home') }}</a>
	</p>


@endsection