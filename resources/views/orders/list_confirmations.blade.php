@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.orders.confirmations.title_list')]) }} @endsection

@section('user-content')

	@include('layout.alert')

	<h2>{{ trans('admin.orders.confirmations.title_list') }}</h2>

	@if (!$pcs->count() && Request::input('s'))
		<div style="margin-bottom:15px;">
			{!! Form::open(['url' => '/orders/confirmations/all', 'class' => 'form-inline pull-right', 'method' => 'get']) !!}
				@if (count($pcs))
					<label class="control-label">
						{{ trans('app.pagination', ['first' => $pcs->firstItem(), 'last' => $pcs->lastItem(), 'total' => $pcs->total()]) }}
					</label>
				@endif
				<div class="input-group">
					{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						<button type="submit" class="btn btn-default">Go</button>
					</span>
				</div>
			{!! Form::close() !!}
			<div class="clearfix"></div>
		</div>
	@endif

	@if ($pcs->count())
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th style="width:150px;">{{ trans('admin.orders.confirmations.order_no') }}</th>
					<th>{{ trans('admin.orders.confirmations.from_account') }}</th>
					<th>{{ trans('admin.orders.confirmations.to_account') }}</th>
					<th style="width:150px;">{{ trans('admin.orders.confirmations.created_at') }}</th>
					<th style="width:150px;"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($pcs as $pc)
				<tr>
					<td><a href="/orders/{{ $pc->order_no }}?ref={!! urlencode(Request::fullUrl()) !!}">{{ $pc->order_no }}</a></td>
					<td>
						{{ $pc->bank }} - {{ $pc->account_no }}<br />
						({{ $pc->account_name }})
					</td>
					<td>
						{{ $pc->to_account }} / IDR {{ number_format($pc->amount, 0, '.', ',') }}<br />
						{{ $pc->transfer_date_label }} / Ref No. {{ $pc->ref_no or "Unspecified" }}
					</td>
					<td>{{ $pc->created_at_label }}</td>
					<td>
						<a href="#" data-toggle="modal" data-target="#deleteModal" data-id="{{ $pc->id }}">{{ trans('admin.orders.confirmations.delete') }}</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	@else
		<p>{{ trans('admin.orders.confirmations.empty') }}</p>
	@endif

	<p style="margin-top:30px;">
		<a href="/my">{{ trans('account.action.home') }}</a>
	</p>

@endsection

@section('modals')

	<div class="modal fade" id="deleteModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h1>Delete Confirmation Data</h1>
					<p>
						Delete this confirmation data? This action cannot be undone.
					</p>
					<div class="text-right" style="margin-top:30px;">
						{!! Form::open(['url' => '/orders/confirmations', 'method' => 'delete']) !!}
							{!! Form::hidden('redirect', Request::fullUrl()) !!}
							{!! Form::hidden('id', null) !!}
							<a href="#" class="btn" data-dismiss="modal">Cancel</a>
							<button type="submit" class="btn btn-default">Delete</button>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')

	<script type="text/javascript">
		$("#deleteModal").on('show.bs.modal', function (event) {
			$(this).find('[name=id]').val($(event.relatedTarget).data('id'));
		});
	</script>
@endsection