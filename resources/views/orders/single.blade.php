@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.orders.single.title', ['order_no' => $order->order_no])]) }} @endsection

@section('user-content')

	@include('layout.alert')

	@if (count($errors))
		<div class="alert alert-danger">{{ $errors->first() }}</div>
	@endif

	<h2>
		{{ trans('admin.orders.single.title', ['order_no' => $order->order_no]) }}

		@if (in_array($order->status, ['success', 'settled']))
			<span class="label label-success pull-right">{{ trans('subscription.order.status.approved') }}</span>
		@elseif ($order->status == 'pending')
			<span class="label label-warning pull-right">{{ trans('subscription.order.status.' . $order->status) }}</span>
		@else
			<span class="label label-danger pull-right">{{ trans('subscription.order.status.' . $order->status) }}</span>
		@endif
	</h2>

	<table class="table table-bordered">
		<tbody>
			<tr>
				<th>Ordered by</th>
				<td>{{ $order->user->name }} ({{ $order->user->email }})</td>
			</tr>
			<tr>
				<th>Ordered on</th>
				<td>{{ $order->created_at }}</td>
			</tr>
			@if ($order->settled_at)
				<tr>
					<th>Settled on</th>
					<td>{{ $order->settled_at }}</td>
				</tr>
			@endif
			<tr>
				<th>Payment Method</th>
				<td>{{ trans('subscription.subscribe.payment_method.' . $order->method) }}</td>
			</tr>
			<tr>
				<th>Total</th>
				<td>IDR {{ number_format($order->total, 0, '.', ',') }}</td>
			</tr>
			<tr>
				<th>Subscription</th>
				<td>{{ $order->subscription->label }}</td>
			</tr>
		</tbody>
	</table>

	<p>
		@if ($order->status == 'pending')
			@can('general', 'accounts.orders.approve')
				<button type="button" class="btn btn-default" id="approveBtn" data-target="#approveModal" data-toggle="modal">Approve</button>
			@endcan
			@can('general', 'accounts.orders.cancel')
				<button type="button" class="btn btn-default" id="cancelBtn">Cancel</button>
			@endcan
		@elseif (in_array($order->status, ['success', 'settled']))
			<a href="/orders/invoices/{{ $order->order_no }}" class="btn btn-default" target="_blank">Print Invoice</a>
		@endif

		@if (!in_array($order->status, ['success', 'settled']))
			@can('general', 'accounts.orders.delete')
				<button type="button" class="btn btn-default" id="deleteBtn" data-target="#deleteModal" data-toggle="modal">Delete</button>
			@endcan
		@endif
	</p>


	<p style="margin-top:30px;">
		@if (Request::input('ref') == 'account')
			<a href="/users/{!! urlencode($order->user->email) !!}">{{ trans('admin.users.action.back_details') }}</a>
		@elseif (Request::input('ref'))
			<a href="{!! Request::input('ref') !!}">{{ trans('app.action.back') }}</a>
		@else
			<a href="/orders/all">{{ trans('admin.orders.action.back_list') }}</a>
		@endif
	</p>

@endsection

@section('modals')

	@if ($order->status == 'pending')
		@can('general', 'accounts.orders.approve')
			<div class="modal fade" id="approveModal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							{!! Form::open(['url' => '/orders/action/approve']) !!}
								<h1>Approve Order Payment</h1>
								<p>Please type in the order number {{ $order->order_no }} to approve order payment. This action cannot be undone.</p>
								{!! Form::hidden('ref', Request::input("ref")) !!}
								{!! Form::hidden('pre_order_no', $order->order_no) !!}
								{!! Form::text('order_no', null, ['class' => 'form-control']) !!}

								<p class="text-right" style="margin-top:30px;">
									<button type="submit" class="btn btn-default">Confirm</button>
									<a class="btn" href="#" data-dismiss="modal">Cancel</a>
								</p>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		@endcan
	@endif
	@if (!in_array($order->status, ['success', 'settled']))
		@can('general', 'accounts.orders.delete')
			<div class="modal fade" id="deleteModal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							{!! Form::open(['url' => '/orders/action/delete']) !!}
								<h1>Delete Order</h1>
								<p>Please type in the order number {{ $order->order_no }} to delete this order. This action cannot be undone.</p>
								{!! Form::hidden('ref', Request::input("ref")) !!}
								{!! Form::hidden('pre_order_no', $order->order_no) !!}
								{!! Form::text('order_no', null, ['class' => 'form-control']) !!}

								<p class="text-right" style="margin-top:30px;">
									<button type="submit" class="btn btn-default">Confirm</button>
									<a class="btn" href="#" data-dismiss="modal">Cancel</a>
								</p>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		@endcan
	@endif

@endsection