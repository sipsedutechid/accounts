@extends('layout.print')

@section('title') Invoice: Order No. {{ $order->order_no }} @endsection

@section('content')

	<table>
		<tbody>
			<tr>
				<td style="padding-top:30px;border:none;" colspan="3">
					<p><img src="{{ asset('/assets/img/company-logo.png') }}" height="70" /></p>
				</td>
			</tr>
			<tr>
				<td style="vertical-align:top;">
					<h1 style="font-family:Helvetica, sans-serif;">
						Invoice
					</h1>
					<p class="text-muted">
						Order No: {{ $order->order_no }}
						<span class="text-success">INVOICE PAID</span>
					</p>
					<p>
						<br />
						Ordered on {{ $order->created_at }}<br />
						Paid on {{ $order->settled_at }}
					</p>
				</td>
				<td style="padding-top:20px;vertical-align:top;" width="33%">
					<h4 style="font-family:Helvetica, sans-serif;">Billed by</h4>
					<p>{{ \App\AppSetting::find('company.name')->value }}</p>
					<p>
						{{ \App\AppSetting::find('company.address_1')->value }}<br />
						{{ \App\AppSetting::find('company.address_2')->value }}<br />
						{{ \App\AppSetting::find('company.city')->value }} - {{ \App\AppSetting::find('company.postal_code')->value }}<br />
						{{ \App\AppSetting::find('company.province')->value }}, {{ \App\AppSetting::find('company.country')->value }}
					</p>

					<p>
						Phone: {{ \App\AppSetting::find('company.phone')->value }}<br />
						Fax: {{ \App\AppSetting::find('company.fax')->value }}<br />
						Email: {{ \App\AppSetting::find('company.email')->value }}<br />
						
					</p>
				</td>
				<td style="padding-top:20px;vertical-align:top;" width="33%">
					<h4 style="font-family:Helvetica, sans-serif;">Billed To</h4>
					<p>{{ strtoupper($order->user->name) }}</p>
					<p>
						{{ $order->user->userDetail->address_1 }}<br />
						{{ $order->user->userDetail->address_2 }}<br />
						{{ $order->user->userDetail->city }} - {{ $order->user->userDetail->postal_code }}<br />
					</p>
					<p>
						Phone: {{ $order->user->userDetail->phone }}<br />
						Email: {{ $order->user->email }}
					</p>
				</td>
			</tr>
		</tbody>
	</table>

	<h3 class="page-header" style="font-family:Helvetica, sans-serif;">Ordered Items</h3>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Subscription</th>
				<th colspan="2">Subtotal</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{ $order->subscription->label }}</td>
				<td style="width:50px;border-right:none;">IDR</td>
				<td class="text-right" style="width:100px;border-left:none;">{{ number_format($order->subscription->price, 0, '.', ',') }}</td>
			</tr>
		</tbody>
	</table>

@endsection