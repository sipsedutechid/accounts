@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('subscription.action.subscribe')]) }} @endsection

@section('user-content')
	
	<h2>{{ trans('subscription.action.subscribe') }}</h2>
	@include('layout.alert')

	{!! Form::open(['url' => '/subscribe']) !!}
		
		<h4 class="page-header" style="margin-top:30px;">Paket Berlangganan</h4>
		<table class="table table-bordered table-hover" style="margin-bottom:30px;">
			<tbody>
				@foreach($subscriptions as $sub)
					<tr>
						<td class="text-center" style="width:50px;">{!! Form::radio('subscription', $sub->name, $sub->name == Request::input('sub')) !!}</td>
						<td>
							{{ $sub->label }} 
							({{ $sub->duration . ' ' . str_singular($sub->period) }})
							- Rp. {{ number_format($sub->price, 0, '.', ',') }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		
		<h4 class="page-header" style="margin-top:30px;">{{ trans('subscription.subscribe.payment_method.title') }}</h4>
		<div style="margin-bottom:30px;">
			<label>{!! Form::radio('method', 'vtweb') !!} {{ trans('subscription.subscribe.payment_method.vtweb') }}</label><br />
			<label>{!! Form::radio('method', 'vtwebinstallment') !!} {{ trans('subscription.subscribe.payment_method.vtwebinstallment') }}</label><br />
			<label>{!! Form::radio('method', 'banktransfer') !!} {{ trans('subscription.subscribe.payment_method.banktransfer') }}</label>
		</div>

		<h4 id="vtDisclaimer">
			{{ trans('subscription.subscribe.veritrans_disclaimer') }}<br />

			<img src="{{ asset('/assets/img/midtrans/midtrans-master.png') }}" height="50" style="padding-top:10px;" />&ensp;
			<img src="{{ asset('/assets/img/veritrans/cc_options.png') }}" height="25" />
		</h4>

		<h4 id="vtInstDisclaimer">
			{{ trans('subscription.subscribe.veritrans_disclaimer') }}<br />
			<img src="{{ asset('/assets/img/midtrans/midtrans-master.png') }}" height="50" style="padding-top:10px;" />&ensp;
			<img src="{{ asset('/assets/img/veritrans/installment_options.png') }}" height="25" />
		</h4>
		<div id="banktransferInstructions">
			<h4 style="margin-bottom:10px;">Make transfer to the following account within 48 hours:</h4>
			<p>{!! nl2br(\App\AppSetting::find('checkout.banktransfer.instructions')->value) !!}</p>
			<img src="{{ asset('/assets/img/veritrans/banktransfer_options.png') }}" height="30" />
		</div>

		<p><div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_SITE') }}"></div></p>

		<p style="margin-top:30px;">
			<button type="submit" class="btn btn-default" id="vtCheckout">{{ trans('subscription.action.checkout_vt') }}</button>
			<button type="submit" class="btn btn-default" id="vtInstCheckout">{{ trans('subscription.action.checkout_vt') }}</button>
			<button type="submit" class="btn btn-default" id="checkout">{{ trans('subscription.action.checkout') }}</button>
		</p>

	{!! Form::close() !!}

	<p style="margin-top:30px;">
		<a href="/my/subscriptions">{{ trans('account.action.back_subscriptions') }}</a>
	</p>

@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('/assets/js/numberformat.js') }}"></script>
	<script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript">
		$(document).ready(function () {
			var _methods = $("[name=method]");
			_methods.on('change', function() { _updateDisclaimer() });
			$("#vtDisclaimer, #vtInstDisclaimer, #banktransferInstructions, #vtCheckout, #vtInstCheckout, #checkout").hide();

			function _updateDisclaimer() {
				var _selectedMethod = $("[name=method]:checked");
				$("#vtDisclaimer, #vtCheckout").toggle(_selectedMethod.val() == "vtweb");
				$("#vtInstDisclaimer, #vtInstCheckout").toggle(_selectedMethod.val() == "vtwebinstallment");
				$("#banktransferInstructions, #checkout").toggle(_selectedMethod.val() == "banktransfer");

			}
			_methods.change();
		});
	</script>

@endsection