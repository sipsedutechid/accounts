@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.users.register.title_' . ($user->id ? 'edit' : 'new'))]) }} @endsection

@section('stylesheets')

	<link rel="stylesheet" type="text/css" href="{{ asset('/bower_components/Strength.js/src/strength.css') }}" />
	<style type="text/css">
		[name=password] { border-bottom-left-radius: 0; border-bottom-right-radius: 0; }
		.button_strength { display: none; }
		.strength_meter { float: left; width:100%; }
	</style>

@endsection

@section('user-content')

	@include('layout.alert')

	<h2>{{ trans('admin.users.register.title_' . ($user->id ? 'edit' : 'new')) }}</h2>

	{!! Form::model($user, ['url' => '/users/' . ($user->id ? 'edit' : 'register'), 'class' => 'form-horizontal']) !!}

		@if (Request::input('ref'))
			{!! Form::hidden('redirect', Request::input('ref')) !!}
		@endif
		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('admin.users.register.fields.email') }}</label>
			@if ($user->id)
				<div class="col-lg-8">
					{!! Form::hidden('email') !!}
					<p class="form-control-static">{{ $user->email }}</p>
				</div>
			@else
				<div class="col-lg-8">
					{!! Form::email('email', null, ['class' => 'form-control']) !!}
				</div>
			@endif
		</div>

		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('admin.users.register.fields.name') }}</label>
			<div class="col-lg-8">{!! Form::text('name', null, ['class' => 'form-control']) !!}</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('admin.users.register.fields.password_' . ($user->id ? 'change' : 'new')) }}</label>
			<div class="col-lg-4">{!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('admin.users.register.fields.password_confirmation') }}</label>
			<div class="col-lg-4">{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3 col-sm-4">{{ trans('admin.users.register.fields.role') }}</label>
			<div class="col-lg-8">{!! Form::select('role', \App\Role::dropdown(), $user->role_id ?: 3, ['class' => 'form-control']) !!}</div>
		</div>

		<div class="form-group">
			@if (!$user->verified_at)
				<div class="col-lg-8 col-lg-offset-4">
					<div class="checkbox">
						<label>{!! Form::checkbox('autoverify') !!} {{ trans('admin.users.register.fields.autoverify') }}</label>
					</div>
				</div>
			@endif

			@if (!$user->id)
				<div class="col-lg-8 col-lg-offset-4">
					<div class="checkbox">
						<label>{!! Form::checkbox('send_welcome_email') !!} {{ trans('admin.users.register.fields.send_welcome_email') }}</label>
					</div>
				</div>
			@endif
		</div>

		@if ($user->id)
			<!-- PROFESSIONAL DETAILS -->
			<h4 class="page-header" style="margin-top:30px;margin-bottom:15px;">{{ trans('admin.users.register.prof_details') }}</h4>
			<div class="form-group">
				<label class="col-lg-3 col-sm-4 control-label">Profession</label>
				<div class="col-lg-8">
					<div class="radio">
						<label>{!! Form::radio('profession', 'doctor', $user->profession == 'doctor') !!} Doctor</label>&ensp;
						<label>{!! Form::radio('profession', 'dentist', $user->profession == 'dentist') !!} Dentist</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 col-sm-4 control-label">IDI/PDGI No.</label>
				<div class="col-lg-4">
					{!! Form::text('id_no', $user->id_no ?: null, ['class' => 'form-control']) !!}
				</div>
			</div>

			<!-- BILLING DETAILS -->
			<h4 class="page-header" style="margin-top:30px;margin-bottom:15px;">{{ trans('admin.users.register.billing_details') }}</h4>
			<div class="form-group">
				<label class="control-label col-lg-3 col-sm-4">{{ trans('account.settings.billing.address_1') }}</label>
				<div class="col-lg-8"> 
					{!! Form::text('address_1', $user->userDetail ? $user->userDetail->address_1 : null, ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-lg-3 col-sm-4">{{ trans('account.settings.billing.address_2') }}</label>
				<div class="col-lg-8"> 
					{!! Form::text('address_2', $user->userDetail ? $user->userDetail->address_2 : null, ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-lg-3 col-sm-4">{{ trans('account.settings.billing.city') }}</label>
				<div class="col-lg-4"> 
					{!! Form::text('city', $user->userDetail ? $user->userDetail->city : null, ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-lg-3 col-sm-4">{{ trans('account.settings.billing.postal_code') }}</label>
				<div class="col-lg-4"> 
					{!! Form::text('postal_code', $user->userDetail ? $user->userDetail->postal_code : null, ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-lg-3 col-sm-4">{{ trans('account.settings.billing.phone') }}</label>
				<div class="col-lg-4"> 
					{!! Form::text('phone', $user->userDetail ? $user->userDetail->phone : null, ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-lg-3 col-sm-4">{{ trans('account.settings.billing.country_code') }}</label>
				<div class="col-lg-4"> 
					{!! Form::select('country', ['IDN' => 'Indonesia'], $user->userDetail ? $user->userDetail->country_code : null, ['class' => 'form-control']) !!}
				</div>
			</div>

			<!-- CURRENT SUBSCRIPTION -->
			<h4 class="page-header" style="margin-top:30px;margin-bottom:15px;">{{ trans('admin.users.register.subscription') }}</h4>
			@if ($subscription)
				<p>{{ trans('subscription.has_subscription', ['expire' => date('j M Y', strtotime($subscription->expired_at))]) }}</p>
			@else
				<p>
					{{ trans('admin.users.register.not_subscribed') }}
					<a href="/users/{!! urlencode($user->email) !!}/subscribe">{{ trans('admin.users.register.action.subscribe') }}</a>
				</p>
			@endif

			<!-- ORDER HISTORY -->
			<h4 class="page-header" style="margin-top:30px;margin-bottom:15px;">{{ trans('subscription.self.orders.title') }}</h4>
			@if (count($orders) > 0)
				<table class="table table-condensed">
					<thead>
						<tr>
							<th style="width:150px;">{{ trans('subscription.self.orders.order_no') }}</th>
							<th>{{ trans('subscription.self.orders.subscription') }}</th>
							<th class="text-right">{{ trans('subscription.self.orders.total') }}</th>
							<th>{{ trans('subscription.self.orders.status') }}</th>
						</tr>
					</thead>
					<tbody>
						@foreach($orders as $order)
							<tr>
								<td><a href="/orders/{{ $order->order_no }}?ref=account">{{ $order->order_no }}</td>
								<td>{{ $order->subscription->label }}</td>
								<td class="text-right">IDR {{ number_format($order->total, 0, '.', ',') }}</td>
								<td>
									{{ trans('subscription.order.status.' . $order->status) }}
									({{ trans('subscription.subscribe.payment_method.' . $order->method) }})
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<p>{{ trans('admin.users.register.orders_empty') }}</p>
			@endif

		@endif

		<div style="margin-top:30px;">
			<button type="submit" class="btn btn-default">{{ trans('admin.users.register.fields.submit_' . ($user->id ? 'change' : 'new')) }}</button>
			<button type="button" class="btn" id="deactivateBtn" data-toggle="modal" data-target="#deactivateModal">{{ trans('account.action.deactivate') }}</button>
		</div>

		<p style="margin-top: 30px;">
			@if (Request::input('ref') == 'list')
				<a href="/users/all">{{ trans('admin.users.action.back_list') }}</a>
			@else
				<a href="/my">{{ trans('account.action.home') }}</a>
			@endif
		</p>

	{!! Form::close() !!}

@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('/bower_components/Strength.js/src/strength.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$("#password").strength();
		});
	</script>

@endsection