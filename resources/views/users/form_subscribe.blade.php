@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.users.subscribe.title')]) }} @endsection

@section('stylesheets')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/user.css') }}" />
@endsection

@section('user-content')

	@if (count($errors))
		<div class="alert alert-danger">{{ $errors->first() }}</div>
	@endif

	<h1>{{ trans('admin.users.subscribe.title') }}</h1>

	{!! Form::open(['url' => (isset($group) ? '/users/groups/subscribe' : '/users/subscribe'), 'class' => 'form-horizontal']) !!}
	@if (isset($group))
		{!! Form::hidden('redirect', Request::input('ref')) !!}
		{!! Form::hidden('group', $group->slug) !!}
		<div class="form-group">
			<label class="control-label col-sm-3">{{ trans('admin.users.groups.name') }}</label>
			<div class="col-sm-9">
				<p class="form-control-static">{{ $group->name }}</p>
			</div>
		</div>
	@else
		{!! Form::hidden('email', $user->email) !!}
		<div class="form-group">
			<label class="control-label col-sm-3">{{ trans('admin.users.subscribe.account') }}</label>
			<div class="col-sm-9">
				<p class="form-control-static">{{ $user->email }}</p>
			</div>
		</div>
	@endif

		<div class="form-group">
			<div class="col-sm-9 col-sm-offset-3">
				<div class="checkbox">
					<label>{!! Form::checkbox('fixed_dates') !!} {{ trans('admin.users.subscribe.fixed_dates') }}</label>
					<span class="help-block small">{{ trans('admin.users.subscribe.fixed_dates_help') }}</span>
				</div>
			</div>
		</div>

		<div class="form-group" data-end-current="true">
			<label class="control-label col-sm-3">{{ trans('admin.users.subscribe.date_start') }}</label>
			<div class="col-sm-5">
				{!! Form::date('starts_at', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group" data-end-current="true">
			<label class="control-label col-sm-3">{{ trans('admin.users.subscribe.date_end') }}</label>
			<div class="col-sm-5">
				{!! Form::date('expired_at', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group" data-end-current="false">
			{!! Form::hidden('period', 'days') !!}
			<label class="control-label col-sm-3">{{ trans('admin.users.subscribe.duration') }}</label>
			<div class="col-sm-5 col-sm-4">
				<div class="input-group">
					{!! Form::number('duration', 1, ['class' => 'form-control text-right', 'min' => 1]) !!}
					<span class="input-group-btn btn-group" id="selectPeriod">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ trans('admin.users.subscribe.duration_days') }} <b class="caret"></b></button>
						<ul class="dropdown-menu pull-right">
							<li><a href="#" data-period="days">{{ trans('admin.users.subscribe.duration_days') }}</a></li>
							<li><a href="#" data-period="weeks">{{ trans('admin.users.subscribe.duration_weeks') }}</a></li>
							<li><a href="#" data-period="months">{{ trans('admin.users.subscribe.duration_months') }}</a></li>
							<li><a href="#" data-period="years">{{ trans('admin.users.subscribe.duration_years') }}</a></li>
						</ul>
					</span>
				</div>
			</div>
		</div>

		<p style="margin-top: 30px;"><button type="submit" class="btn btn-default">{{ trans('admin.users.subscribe.save') }}</button></p>

		<p style="margin-top: 30px;">
			@if (Request::input('ref'))
				<a href="{!! Request::input('ref') !!}">{{ trans('app.action.back') }}</a>
			@else
				<a href="/users/{!! urlencode($user->email) !!}">{{ trans('admin.users.action.back_details') }}</a>
			@endif
		</p>

	{!! Form::close() !!}

@endsection

@section('scripts')

	<script type="text/javascript">
		$(document).ready(function () {
			var _endCurrent 	= $("[name=fixed_dates]");
				_durationSelect = $("#selectPeriod");
				_period 		= $("[name=period]");

			_endCurrent.on('change', function () {
				$("[data-end-current=true]").toggle($(this).is(":checked"));
				$("[data-end-current=false]").toggle(!$(this).is(":checked"));
			});
			_durationSelect.find("ul>li>a").on('click', function (e) {
				e.preventDefault();
				_durationSelect.find('button').text($(this).text() + " ").append($("<b />").addClass('caret'));
				_period.val($(this).data('period'));
			});
			_endCurrent.change();

			@if (old('period'))
				_durationSelect.find("a[data-period={{ old('period') }}]").click();
			@endif
		});

	</script>

@endsection