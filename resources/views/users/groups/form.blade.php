@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.users.groups.title_new')]) }} @endsection

@section('user-content')

	<div ng-app="userGroupFormApp">
		
		<div ng-controller="rootCtrl as root">
			<h2>{{ trans('admin.users.groups.title_new') }}</h2>
			<br />
			{!! Form::open(['url' => 'users/groups/create', 'class' => 'form-horizontal']) !!}

				<div class="form-group">
					<label class="control-label col-lg-2 col-sm-3">Group Name</label>
					<div class="col-sm-6">
						{!! Form::text('name', null, ['class' => 'form-control', 'ng-model' => 'name', 'autofocus' => 'autofocus']) !!}
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2 col-sm-3">Description</label>
					<div class="col-sm-6">
						{!! Form::textarea('description', null, ['rows' => 3, 'ng-model' => 'description', 'class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group" ng-controller="membersListCtrl as membersList">
					<label class="control-label col-lg-2 col-sm-3">Members</label>
					<div class="col-sm-9">
						<div style="position:relative;">
							<input type="text" name="term" ng-model="membersList.searchTerm" class="form-control" ng-blur="membersList.options = []" ng-keyup="membersList.keyup($event)" placeholder="Type a name or email.." autocomplete="off" />
							<ul class="list-group" style="position:absolute;top:100%;left:0;z-index:1024;">
								<li ng-repeat="option in membersList.options track by $index" ng-class="['list-group-item', {'active': membersList.hoverIndex == $index}]" ng-mouseup="membersList.select(option)">
									<% option.name %> (<% option.email %>)
								</li>
							</ul>
						</div>
						<br />
						<div class="well" style="max-height:400px;min-height:200px;overflow-y:auto;">
							<ul class="list-group">
								<li class="list-group-item" ng-repeat="member in $parent.members track by $index">
									<% member.name %> (<% member.email %>)
									<span class="pull-right">
										<a ng-if="member.isAdmin !== true" ng-click="membersList.adminize($index)">Make Admin</a>
										<span ng-if="member.isAdmin == true" class="label label-success">Group Admin</span>
										<a ng-click="membersList.remove($index)">Remove</a>
									</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<button type="button" class="btn btn-primary" ng-click="save()">Save Group</button>

				<p style="margin-top:30px;">
					<a href="/users/groups/all">Back to Groups List</a>
				</p>

			</div>

		{!! Form::close() !!}
	</div>

@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('/bower_components/angular/angular.min.js') }}"></script>
	<script type="text/javascript">
		
		angular.module('userGroupFormApp', [], function ($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		})

		// Root controller
		.controller('rootCtrl', function ($http, $window, $scope) {
			$scope.name = "";
			$scope.description = "";
			$scope.members = [];
			$scope.save = function () {
				$http.post('/api/user-groups/save', {
					name: $scope.name,
					description: $scope.description,
					members: $scope.members
				}).success(function () { $window.location.href = '/users/groups/all' });
			};
		})

		// Members list controller
		.controller('membersListCtrl', function ($scope, $http, $timeout) {
			var vm = this;
			vm.options = [];
			vm.hoverIndex = -1;
			vm.searchTerm = "";

			vm.keyup = function (event) {
				if (event.keyCode == 13 && vm.hoverIndex > -1) { return vm.select(vm.options[vm.hoverIndex]); }
				if (event.keyCode == 40 && vm.hoverIndex < vm.options.length - 1) { return vm.hoverIndex++; }
				if (event.keyCode == 38 && vm.hoverIndex > -1) { return vm.hoverIndex--; }

				if (angular.isDefined(vm.searchPromise)) $timeout.cancel(vm.searchPromise);
				vm.searchPromise = $timeout(function () {
					if (vm.searchTerm != "") $http.get('/api/users/search?term=' + vm.searchTerm).success(function (r) { vm.options = r; });
					else vm.options = [];
				}, 300);
				return;
			};

			vm.adminize = function (index) {
				for (var i = $scope.$parent.members.length - 1; i >= 0; i--) {
					$scope.$parent.members[i].isAdmin = false;
				}
				return $scope.$parent.members[index].isAdmin = true;
			};

			vm.remove = function (index) {
				return $scope.$parent.members.splice(index, 1);
			};

			vm.select = function (option) {
				if (!$scope.$parent.members.length)
					option.isAdmin = true;
				for (var i = $scope.$parent.members.length - 1; i >= 0; i--) {
					if ($scope.$parent.members[i].email == option.email) return ;
				}
				$scope.$parent.members.push(option);
				vm.options = [];
				vm.hoverIndex = -1;
				vm.searchTerm = "";
			};
		});

	</script>
@endsection