@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.users.groups.list.title')]) }} @endsection

@section('user-content')

	@include('layout.alert')

	<h2>{{ trans('admin.users.groups.list.title') }}</h2>

	<div style="margin-bottom:15px;">
		<span class="pull-left">
			<a href="/users/groups/create?ref=list" class="btn btn-primary">
			{{ trans('admin.users.groups.title_new') }}
		</a>
		</span>
		{!! Form::open(['url' => '/users/groups/all', 'class' => 'form-inline pull-right', 'method' => 'get']) !!}
			@if (count($groups))
				<label class="control-label">
					{{ trans('app.pagination', ['first' => $groups->firstItem(), 'last' => $groups->lastItem(), 'total' => $groups->total()]) }}
				</label>
			@endif
			<div class="input-group">
				{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => 'Search groups']) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default">Go</button>
				</span>
			</div>
		{!! Form::close() !!}
		<div class="clearfix"></div>
	</div>

	@if (count($groups) > 0)
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Admin User</th>
					<th style="width:100px;">Members</th>
					<th style="width:150px;">Created</th>
				</tr>
			</thead>
			<tbody>
				@foreach($groups as $group)
					<tr>
						<td><a href="/users/groups/{{ $group->slug }}">{{ $group->name}}</a></td>
						<td>{{ $group->adminUser->name }}</td>
						<td>{{ $group->members_count }}</td>
						<td>{{ $group->created_at }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		{!! $groups->appends(['s' => Request::input('s')])->links() !!}
	@else
		<p>{{ trans('admin.users.groups.list.empty') }}</p>
	@endif

	<p>
		<a href="/my">{{ trans('account.action.home') }}</a>
	</p>

@endsection