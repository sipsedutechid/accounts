@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => $group->name ]) }} @endsection

@section('user-content')

	<div ng-app="singleUserGroupApp">
		
		<h2>
			{{ $group->name }}
			@if ($group->description)
				<br />
				<span class="small text-muted">{{ $group->description }}</span>
			@endif
		</h2>
		<p>
			Group created at {{ $group->created_at }}
		</p>

		<h4 class="page-header">{{ trans_choice('admin.users.groups.members_count', $group->members_count, ['count' => $group->members_count]) }}</h4>
		<div ng-controller="listCtrl">
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>Account</th>
						<th>Member Since</th>
						@can ('general', 'accounts.user_groups.manage')
							<th style="width:200px;"></th>
						@endcan
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="member in members">
						<td>
							<% member.name %> (<% member.email %>)
						</td>
						<td ng-bind="member.pivot.created_at_label"></td>
						@can ('general', 'accounts.user_groups.manage')
							<td>
								<span ng-if="member.email != groupAdminEmail">
									<span ng-if="member.hasConfirm == true">
										Confirm? <a ng-click="confirm(member)">Yes</a> / <a ng-click="cancel(member)">No</a>
									</span>
									<span ng-if="member.hasConfirm == false">
										<a ng-click="initAction(member, 'remove')">Remove</a>
										/ <a ng-click="initAction(member, 'adminize')">Make Admin</a>
									</span>
								</span>
							</td>
						@endcan
					</tr>
				</tbody>
			</table>

			<div class="row">
				<div class="col-md-5 col-sm-6">
					<label>Add New Member</label>
					<div ng-controller="addMemberCtrl" style="position:relative;">
						<input type="text" ng-model="term" class="form-control" ng-blur="options = []" ng-keyup="keyup($event)" placeholder="Type name or email.." />
						<div class="list-group" style="position:absolute;left:0;top:100%;">
							<a href="#" ng-mouseup="select(option)" ng-repeat="option in options track by $index" ng-class="['list-group-item', 'small', {'active': hoverIndex == $index}]" style="border-top-left-radius:0;border-top-right-radius:0;">
								<% option.name %>
								(<% option.email %>)
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div style="margin-top:30px;">
			@can('general', 'accounts.user_groups.delete')
				<button type="button" data-toggle="modal" data-target="#deleteModal" class="btn btn-default">Delete User Group</button>
			@endcan
			@can('general', 'accounts.users.subscription.subscribe')
				<a href="/users/groups/{{ $group->slug }}/subscribe?ref={!! urlencode(Request::fullUrl()) !!}" class="btn btn-default">Subscribe</a>
			@endcan
		</div>

		<p style="margin-top:30px;"><a href="/users/groups/all">{{ trans('admin.users.groups.action.back_list') }}</a></p>
	</div>

@endsection

@section('modals')
	<div class="modal fade" id="deleteModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h1>Delete User Group</h1>
					<p>Delete {{ $group->name }} user group? All existing subscriptions for members in this group will persist. This action cannot be undone.</p>

					<div class="text-right">
						{!! Form::open(['url' => '/users/groups/' . $group->slug, 'method' => 'delete']) !!}
						<a href="#" data-dismiss="modal" class="btn">Cancel</a>
						<button class="btn btn-default" type="submit">Confirm Delete</button>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

	<script type="text/javascript" src="{{ asset('/bower_components/angular/angular.min.js') }}"></script>
	<script type="text/javascript">
		angular.module('singleUserGroupApp', [], function ($interpolateProvider) {
			$interpolateProvider.startSymbol("<%");
			$interpolateProvider.endSymbol("%>");
		}).controller('listCtrl', function ($scope, $http) {
			$scope.members = [];
			$scope.groupAdminEmail = null;
			$scope.currentAction = null;
			$scope.initAction = function (member, action) {
				for (var i = $scope.members.length - 1; i >= 0; i--) {
					$scope.members[i].hasConfirm = false;
				}
				$scope.currentAction = action;
				member.hasConfirm = true;
			};
			$scope.confirm = function (member) {
				$http.post('/api/users/user-group/{{ $group->slug }}/' + $scope.currentAction, {email: member.email}).success(function() { update(); });
			};
			$scope.cancel = function (member) {
				$scope.currentAction = null;
				member.hasConfirm = false;
			};
			function update() {
				$http.get('/api/users/user-group/{{ $group->slug }}').success(function (r) {
					$scope.members = r.members;
					$scope.groupAdminEmail = r.admin_user.email;
					$scope.currentAction = null;
					for (var i = $scope.members.length - 1; i >= 0; i--) {
						$scope.members[i].hasConfirm = false;
					}
				});
			};
			$scope.$on('update-list', function() { update(); });
			update();
		}).controller('addMemberCtrl', function ($scope, $http, $timeout) {
			$scope.options = [];
			$scope.hoverIndex = -1;
			$scope.select = function (option) {
				_select(option.email);
			};
			$scope.keyup = function (event) {
				if (event.keyCode == 13) {
					if ($scope.hoverIndex > -1) _select($scope.options[$scope.hoverIndex].email);
					return;
				}
				if (event.keyCode == 38) {
					if ($scope.hoverIndex > -1) $scope.hoverIndex--;
					$scope.term = $scope.options[$scope.hoverIndex].name;
					return;
				}
				if (event.keyCode == 40) {
					if ($scope.hoverIndex < $scope.options.length - 1) $scope.hoverIndex++;
					$scope.term = $scope.options[$scope.hoverIndex].name;
					return;
				}
				if (angular.isDefined($scope.promise))
					$timeout.cancel($scope.promise);
				$scope.promise = $timeout(function () {
					if (!$scope.term.length)
						return;
					$http.get('/api/users/search?term=' + $scope.term).success(function (r) { 
						$scope.options = r; 
						$scope.hoverIndex = -1;
					});
				}, 300);
			};
			function _select(email) {
				$http
					.post('/api/users/user-group/{{ $group->slug }}/add', {email: email})
					.success(function () {
						$scope.$emit('update-list');
					});
				$scope.hoverIndex = -1;
				$scope.options = [];
			};
		});
	</script>

@endsection