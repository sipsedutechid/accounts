@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.users.list.title')]) }} @endsection

@section('user-content')

	@include('layout.alert')

	<h2>{{ trans('admin.users.list.title') }}</h2>

	<div style="margin-bottom:15px;">
		<span class="pull-left">
			<a href="/users/register?ref=list" class="btn btn-primary">
				{{ trans('admin.users.register.title_new') }}
			</a>
		</span>
		{!! Form::open(['url' => '/users/all', 'class' => 'form-inline pull-right', 'method' => 'get']) !!}
			@if (count($users))
				<label class="control-label">
					{{ trans('app.pagination', ['first' => $users->firstItem(), 'last' => $users->lastItem(), 'total' => $users->total()]) }}
				</label>
			@endif
			<div class="input-group">
				{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => trans('admin.users.list.search')]) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default">Go</button>
				</span>
			</div>
		{!! Form::close() !!}
		<div class="clearfix"></div>
	</div>

	@if (count($users) > 0)
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>{{ trans('admin.users.list.email') }}</th>
					<th>{{ trans('admin.users.list.name') }}</th>
					<th>{{ trans('admin.users.list.groups') }}</th>
					<th>{{ trans('admin.users.list.created_at') }}</th>
					<th>{{ trans('admin.users.list.status') }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
					<tr>
						<td>
							@if (!$user->role->is_super)
							<a href="/users/{{ urlencode($user->email) }}?ref=list">{{ $user->email }}</a>
							@else
								{{ $user->email }}
							@endif
						</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->user_groups_count ?: trans('account.no_group') }}</td>
						<td>{{ $user->created_at }}</td>
						<td>
							{{ trans('account.status.' . $user->getStatus()) }}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		{!! $users->appends(['s' => Request::input('s')])->links() !!}
	@else
		<p>{{ trans('admin.users.list.empty') }}</p>
	@endif

	<p>
		<a href="/my">{{ trans('account.action.home') }}</a>
	</p>

@endsection