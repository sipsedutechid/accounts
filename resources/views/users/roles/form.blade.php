@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.roles.form.title_' . ($role->id ? 'edit' : 'new')) ]) }} @endsection

@section('stylesheets')

	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/user.css') }}" />

@endsection

@section('user-content')

	@include('layout.alert')

	<h1>{{ trans('admin.roles.form.title_' . ($role->id ? 'edit' : 'new')) }}</h1>

	{!! Form::model($role, ['url' => '/roles/' . ($role->id ?: 'create'), 'class' => 'form-horizontal']) !!}
		@if ($role->id)
			{!! Form::hidden('role', $role->id) !!}
		@endif
		<div class="form-group">
			<label class="control-label col-lg-3">{!! trans('admin.roles.form.fields.name') !!}</label>
			<div class="col-lg-6">{!! Form::text('name', null, ['autofocus' => 'autofocus', 'class' => 'form-control']) !!}</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3">{!! trans('admin.roles.form.fields.abilities') !!}</label>
			<div class="col-lg-9">
				<ul>
					@foreach($abilities as $ability)
						<li>
							<label>{!! Form::checkbox('abilities[]', $ability->id) !!} {{ trans('abilities.' . $ability->name) }}</label>
						</li>
					@endforeach
				</ul>
			</div>
		</div>

		<button type="submit" class="btn btn-default">{{ trans('admin.roles.form.submit_' . ($role->id ? 'edit' : 'new')) }}</button>
		<p style="margin-top: 30px;">
			<a href="/roles/all">{{ trans('admin.roles.action.back_list') }}</a>
		</p>

	{!! Form::close() !!}

@endsection