@extends('layout.user')

@section('title') {{ trans('app.page_title', ['title' => trans('admin.roles.list.title')]) }} @endsection

@section('stylesheets')

	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/user.css') }}" />

@endsection

@section('user-content')

	@include('layout.alert')

	<h1>{{ trans('admin.roles.list.title') }}</h1>
	<div style="margin-bottom:15px;">
		<span class="pull-left">
			<a href="/roles/create" class="btn btn-default">
			{{ trans('admin.roles.action.create') }}
		</a>
		</span>
		{!! Form::open(['url' => '/roles/all', 'class' => 'form-inline pull-right', 'method' => 'get']) !!}
			@if (count($roles))
				<label class="control-label">
					{{ trans('app.pagination', ['first' => $roles->firstItem(), 'last' => $roles->lastItem(), 'total' => $roles->total()]) }}
				</label>
			@endif
			<div class="input-group">
				{!! Form::text('s', Request::input('s'), ['class' => 'form-control', 'placeholder' => 'Search roles']) !!}
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default">Go</button>
				</span>
			</div>
			@if (Request::input('s'))<a href="/roles/all" class="btn btn-default">Clear</a>@endif
		{!! Form::close() !!}
		<div class="clearfix"></div>
	</div>

	<table class="table table-hover">
		<thead><tr><th>Role</th><th style="width: 200px;">Last Modified</th></tr></thead>
		<tbody>
			@foreach($roles as $role)
				<tr>
					<td>
						@if (!$role->is_super && !$role->is_user && !$role->is_subscriber)
							<a href="/roles/{{ $role->id }}">{{ $role->name }}</a>
						@else
							{{ $role->name }}
						@endif
					</td>
					<td>
						{{ $role->updated_at }}
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	{!! $roles->appends(['s' => Request::input('s')])->links() !!}

	<p style="margin-top: 30px;">
		<a href="/my">{{ trans('account.action.home') }}</a>
	</p>

@endsection